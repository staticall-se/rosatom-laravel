# Создать авторизацию
```
php artisan make:auth
```

# Применить миграции
```
php artisan migrate
```

# Заполнить БД данными
```
php artisan db:seed
```
