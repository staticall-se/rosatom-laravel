const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js(
    [
        'resources/js/app.js',
        'resources/js/select2.full.min.js',
        'resources/js/jquery-3.5.1.min.js'
    ], 'public/js/select2.js')
    .styles(
        [
            'resources/css/app.css',
            'resources/css/select2.min.css',
            'resources/css/customisation.css',
        ],
        'public/css/style.css')
    .sourceMaps();
