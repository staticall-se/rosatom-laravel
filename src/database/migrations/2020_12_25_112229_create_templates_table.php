<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('templates', function (Blueprint $table) {
            $table->id();
            $table->string('company_id');
            $table->jsonb('params')->nullable();
            $table->timestamps();
        });
        Schema::table(
            'templates',
            function (Blueprint $table) {
                $table->foreign('company_id')->references('id')->on('companies');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::beginTransaction();
        Schema::table(
            'templates',
            function (Blueprint $table) {
                $table->dropForeign('reports_company_id_foreign');
            }
        );
        Schema::dropIfExists('templates');
        DB::commit();
    }
}
