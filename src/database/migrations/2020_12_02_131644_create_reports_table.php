<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateReportsTable extends Migration
{
    public function up(): void
    {
        DB::beginTransaction();
        Schema::create(
            'reports',
            function (Blueprint $table) {
                $table->id();
                $table->string('company_id')->nullable();
                $table->jsonb('data')->nullable();
                $table->timestamps();
            }
        );
        Schema::table(
            'reports',
            function (Blueprint $table) {
                $table->foreign('company_id')->references('id')->on('companies');
            }
        );
        DB::commit();
    }

    public function down(): void
    {
        DB::beginTransaction();
        Schema::table(
            'reports',
            function (Blueprint $table) {
                $table->dropForeign('reports_company_id_foreign');
            }
        );
        Schema::dropIfExists('reports');
        DB::commit();
    }
}
