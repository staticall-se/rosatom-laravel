<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddValueOnVariables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('variables')->insert(
            [
                'key'        => 'percentOfDeviation',
                'value'      => 5,
                'created_at' => date('Y-m-d H:i:s', strtotime('now')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('now'))
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
