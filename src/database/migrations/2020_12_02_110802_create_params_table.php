<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParamsTable extends Migration
{
    public function up(): void
    {
        Schema::create(
            'params',
            function (Blueprint $table) {
                $table->string('id')->primary();
                $table->string('paragraph', 32);
                $table->string('name', 255);
                $table->string('unit', 32)->nullable();
                $table->timestamps();
            }
        );
    }

    public function down(): void
    {
        Schema::dropIfExists('params');
    }
}
