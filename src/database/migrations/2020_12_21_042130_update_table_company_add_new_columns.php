<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableCompanyAddNewColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->renameColumn('name', 'company_name');
            $table->renameColumn('description', 'company_description');
        });

        Schema::table('companies', function (Blueprint $table) {
            $table->string('post_of_chief', 100)->nullable();
            $table->string('fio_of_chief', 100)->nullable();
            $table->string('post_of_contracting_officer', 32)->nullable();
            $table->string('fio_of_contracting_officer', 70)->nullable();
            $table->timestamp('start_duration_contract')->useCurrent();
            $table->timestamp('finish_duration_contract')->useCurrent();
            $table->string('location_address', 70)->nullable();
            $table->string('telephone', 20)->nullable();
            $table->string('fax', 50)->nullable();
            $table->string('email', 32)->nullable();
            $table->string('industry', 32)->nullable();
            $table->string('main_activity', 255)->nullable();
            $table->string('amount_statutory_fund', 32)->nullable();
            $table->string('post_of_executive_director', 100)->nullable();
            $table->string('fio_perpetrator', 100)->nullable();
            $table->string('mobile_perpetrator', 20)->nullable();
            $table->string('email_perpetrator', 32)->nullable();
            $table->timestamp('balance_board_review_date')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
