<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Param;
use App\Models\Template;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TemplateController extends Controller
{
    public function index()
    {
        $reports = $this->treatmentReports(Template::all());

        return view('template.index', compact('reports'));
    }

    protected function treatmentReports(object $reports)
    {
        $treatmentReports = [];

        if (is_countable($reports)) {
            foreach ($reports as $report) {
                $report->company_id = $this->findCompanyForId($report->company_id);
                $report->params     = $this->findParamForId($report->params);
                $treatmentReports[] = $report;
            }
            return $treatmentReports;
        }
        $reports->company_id = $this->findCompanyForId($reports->company_id);
        $reports->params     = $this->findParamForId($reports->params);

        return $reports;
    }

    protected function findCompanyForId(string $id): string
    {
        return Company::all()->where('id', $id)->first()->company_name;
    }

    protected function findParamForId(?array $ids = []): array
    {
        $treatmentParams = [];
        if ($ids === null) {
            return $treatmentParams;
        }
        $params          = Param::whereIn('id', $ids)->get();
        foreach ($params as $item) {
            $treatmentParams[] = $item;
        }
        return $treatmentParams;
    }

    public function create()
    {
        return view(
            'template.create',
            [
                'companies' => Company::all(),
                'params'    => Param::getAllSortedByParagraph(),
            ]
        );
    }

    public function store(Request $request): Response
    {
        $findCompany = Company::where('company_name', $request->get('company_id'))->first();

        $report = new Template(
            [
                'company_id' => $findCompany->id,
                'params'     => $request->get('params'),
            ]
        );

        $report->save();
        return redirect('/template')->with('success', 'Шаблон отчёта сохранен!');
    }

    public function edit($id)
    {
        $report           = $this->treatmentReports(Template::all()->where('id', $id)->first());
        $reportCheckedIds = [];
        foreach ($report->params as $param) {
            $reportCheckedIds[] = $param['id'];
        }

        $params = Param::getAllSortedByParagraph();

        return view('template.edit', compact('report', 'params', 'reportCheckedIds'));
    }

    public function update(Request $request, $id)
    {
        $report = Template::all()->where('id', $id)->first();

        $report->params = $request->get('params');
        $report->save();

        return redirect('/template')->with('success', 'Шаблон отчёта обновлен!');
    }

    public function destroy($id)
    {
        $report = Template::all()->where('id', $id)->first();
        $report->delete();

        return redirect('/template')->with('success', 'Шаблон отчёта удален!');
    }
}
