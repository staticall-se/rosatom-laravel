<?php

namespace App\Http\Controllers;

use App\Aggregator\PlanAggregator;
use App\Models\Data;
use App\Models\Param;
use App\Models\Template;
use App\Models\Variables;
use App\Months;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PlanController extends Controller
{
    private const TYPE_DATA          = 'data';
    private const TYPE_FACT_PREVIOUS       = 'fact_previous';
    private const TYPE_PREDICT_CURRENT = 'predict_current';

    public function __construct()
    {
        $this->middleware('web');
    }

    public function show(Request $request, $id): Renderable
    {
        $currentYear = (int)$request->get('year', date('Y'));

        if (null !== $id) {
            $currentReport = Template::all()->where('company_id', $id)->first();
        }

        $lastYear = Data::all()->where('template_id', $currentReport->id)->where('year', $currentYear - 1)->first();
        $thisYear = Data::all()->where('template_id', $currentReport->id)->where('year', $currentYear)->first();
        $nextYear = Data::all()->where('template_id', $currentReport->id)->where('year', $currentYear + 1)->first();

        $params = Param::all()->whereIn('id', $currentReport->params)->sortBy('paragraph')->keyBy('id')->toArray();

        $aggregator = new PlanAggregator(
            $lastYear->data ?? null,
            $thisYear->data ?? null,
            $nextYear->data ?? null,
            $params,
            $thisYear->fact_previous ?? null,
            $thisYear->predict_current ?? null,
        );

        return view(
            'plan.show',
            [
                'currentYear'   => $currentYear,
                'currentReport' => $currentReport,
                'months'        => Months::get(),
                'years'         => range(date('Y') - 2, date('Y') + 2),
                'reports'       => Template::all(),
                'aggregated'    => $aggregator->aggregate(),
            ]
        );
    }

    public function index(Request $request)
    {
        $ordinal     = 0;
        $companies   = Template::all();
        $currentYear = (int)$request->get('year', date('Y'));

        $reports = [];

        foreach ($companies as $item) {
            ++ $ordinal;
            $data   = Data::all()->where('year', $currentYear)->where('template_id', $item->id)->first();
            $report = Template::all()->where('company_id', $item->company_id)->first();
            if ($data) {
                $report->isCurrentYearPlan = true;
            }
            $report->ordinal = $this->getOrdinal($ordinal);
            $reports[]       = $report;
        }

        return view(
            'plan.index',
            [
                'reports'     => $reports,
                'currentYear' => $currentYear,
                'years'       => range(date('Y') - 2, date('Y') + 2),
            ]
        );
    }

    protected function getOrdinal(int $ordinal)
    {
        switch ($ordinal) {
            case $ordinal < 10 && $ordinal > 0:
                return '000' . $ordinal;
                break;
            case $ordinal >= 10 && $ordinal < 100:
                return '00' . $ordinal;
                break;
            case $ordinal >= 100 && $ordinal < 1000:
                return '0' . $ordinal;
                break;
            default:
                return $ordinal;
        }
    }

    public function create(Request $request)
    {
        return view(
            'plan.create',
            [
                'reports' => Template::all(),
                'years'   => range(date('Y') - 2, date('Y') + 2),
            ]
        );
    }

    public function editPlan(Request $request)
    {
        $percent = Variables::where('key', 'percentOfDeviation')->first();
        $report  = Template::where('company_id', $request->get('id'))->first();

        if ($report->params === null) {
            return redirect('template/' . $report->id . '/edit')->with('error', 'Заполните параметры для шаблона ' . $report->company->company_name);
        }

        $data   = Data::where('template_id', $report->id)->where('year', $request->get('year'))->first();
        $params = Param::all()->whereIn('id', array_keys($report->params))->keyBy('id')->toArray();
        $months = Months::get();
        unset($months[12], $months[13], $months[14], $months[15]);

        $existingData    = [];
        $existingPredict = [];
        $existingFact    = [];

        if (null !== $data) {
            $existingData    = $data->data;
            $existingFact    = $data->fact_previous;
            $existingPredict = $data->predict_current;
        }

        return view(
            'plan.adjustment',
            [
                'months'          => $months,
                'report'          => Param::getSortedParams($this->findParamForId($report->params)),
                'year'            => $request->get('year'),
                'existingData'    => $existingData,
                'existingFact'    => $existingFact,
                'existingPredict' => $existingPredict,
                'params'          => $params,
                'data'            => $data,
                'percent'         => $percent->value,
            ]
        );
    }

    protected function findParamForId(array $ids = []): array
    {
        $params          = Param::whereIn('id', $ids)->get();
        $treatmentParams = [];
        foreach ($params as $item) {
            $treatmentParams[] = $item;
        }
        return $treatmentParams;
    }

    public function step2(Request $request)
    {
        $report = Template::where('company_id', $request->get('id'))->first();
        $data   = Data::where('template_id', $report->id)->where('year', $request->get('year'))->first();

        if ($report->params === null) {
            return redirect('template/' . $report->id . '/edit')->with('error', 'Заполните параметры для шаблона ' . $report->company->company_name);
        }

        $params = Param::all()->whereIn('id', array_keys($report->params))->keyBy('id')->toArray();
        $months = Months::get();
        unset($months[12], $months[13], $months[14], $months[15]);

        $existingData = [];

        if (null !== $data) {
            $existingData = $data->data;
        }

        return view(
            'plan.step2',
            [
                'months'       => $months,
                'report'       => Param::getSortedParams($this->findParamForId($report->params)),
                'year'         => $request->get('year'),
                'existingData' => $existingData,
                'params'       => $params,
                'data'         => $data,
            ]
        );
    }

    public function update(Request $request, $id)
    {
        $data = Data::where('template_id', $id)->where('year', $request->get('year'))->first();

        if (null === $data) {
            return redirect('/plan')->with('error', 'Данных по такому году и шаблону не существует!');
        }

        $newData    = $this->compareData($data->data, $request->get('data'), self::TYPE_DATA);
        $newFact    = $this->compareData($data->fact_previous, $request->get('fact_previous'), self::TYPE_FACT_PREVIOUS);
        $newPredict = $this->compareData($data->predict_current, $request->get('predict_current'), self::TYPE_PREDICT_CURRENT);

        $data->data            = $newData;
        $data->fact_previous   = $newFact;
        $data->predict_current = $newPredict;

        $data->save();

        return redirect('/plan')->with('success', 'Корректировка данных произошла успешно!');
    }

    private function compareData(array $data, array $requestData, string $type): array
    {
        $newData = [];

        foreach (array_keys($data) as $k) {
            $treatedData = $this->treatmentDataItem($data[$k], $requestData[$k], $type);
            $newData[$k] = $treatedData;
        }
        return $newData;
    }

    private function treatmentDataItem(array &$data, array $requestData, string $type): array
    {
        $treatmentDataItem = [];
        $months            = Months::get();
        unset($months[12], $months[13], $months[14], $months[15]);

        if ($type === self::TYPE_DATA) {
            foreach ($months as $key => $value) {
                $newData             = [
                    'plan'               => $this->getPlanValue($data, $requestData, $key),
                    'comment'            => $this->getComment($data, $key),
                    'comment_deviations' => $this->getCommentDeviation($requestData, $key),
                ];
                $treatmentDataItem[] = $newData;
            }
            return $treatmentDataItem;
        }

        if ($type === self::TYPE_FACT_PREVIOUS) {
            $data[self::TYPE_FACT_PREVIOUS] = $this->getFactPreviousValue($data, $requestData);
            return $data;
        }

        if ($type === self::TYPE_PREDICT_CURRENT) {
            $data[self::TYPE_PREDICT_CURRENT] = $this->getPredictCurrentValue($data, $requestData);
            return $data;
        }

        return [];
    }

    private function getPlanValue(array &$data, array $newData, int $monthId): ?float
    {
        return $data[$monthId]['plan'] = $newData[$monthId]['plan'] ?? $data[$monthId]['plan'];
    }

    private function getFactPreviousValue(array &$data, array $newData): ?float
    {
        return $data['fact_previous'] = $newData['fact_previous'] ?? $data['fact_previous'];
    }

    private function getPredictCurrentValue(array &$data, array $newData): ?float
    {
        return $data['predict_current'] = $newData['predict_current'] ?? $data['predict_current'];
    }

    private function getComment(array &$data, int $monthId): ?string
    {
        return $data[$monthId]['comment'];
    }

    private function getCommentDeviation(array &$newData, int $monthId): ?string
    {
        return $newData[$monthId]['comment_deviations'];
    }

    public function store(Request $request): Response
    {
        $template = Template::where('company_id', $request->get('report_id'))->first();

        $data = Data::where('template_id', $template->id)->where('year', $request->get('year'))->first();

        if (null === $data) {
            $data = new Data(
                [
                    'template_id'     => $template->id,
                    'year'            => $request->get('year'),
                    'data'            => $request->get('data'),
                    'fact_previous'   => $request->get('fact_previous'),
                    'predict_current' => $request->get('predict_current'),
                ]
            );
        }

        $data->data            = $request->get('data');
        $data->fact_previous   = $request->get('fact_previous');
        $data->predict_current = $request->get('predict_current');

        $data->save();

        return redirect('/plan')->with('success', 'Данные успешно сохранены!');
    }
}
