<?php

namespace App\Http\Controllers;

use App\Aggregator\PlanAggregator;
use App\Models\Data;
use App\Models\Param;
use App\Models\Report;
use App\Models\Template;
use App\Models\Variables;
use App\Months;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ReportController extends Controller
{
    public function index(Request $request)
    {
        $companies = Template::all();
        $ordinal   = 0;
        $months    = Months::get();
        $month     = date("n") - 1;
        unset($months[12], $months[13], $months[14], $months[15], $months[$month]);

        $currentMonth = Months::get()[$month];

        if ($request->get('month')) {
            $currentMonth = $request->get('month');
        }

        $currentYear = (int)date('Y');

        $reports = [];

        foreach ($companies as $item) {
            ++ $ordinal;
            $data   = Report::all()->where('company_id', $item->company_id)->where('year', $currentYear)->first();
            $report = Template::where('company_id', $item->company_id)->first();
            if ($data) {
                $report->isCurrentYearPlan = true;
            }
            $report->ordinal = $this->getOrdinal($ordinal);
            $reports[]       = $report;
        }

        return view(
            'report.index',
            [
                'companies'    => $companies,
                'currentYear'  => $currentYear,
                'reports'      => $reports,
                'months'       => $months,
                'currentMonth' => $currentMonth,
            ]
        );
    }

    protected function getOrdinal(int $ordinal)
    {
        switch ($ordinal) {
            case $ordinal < 10 && $ordinal > 0:
                return '000' . $ordinal;
                break;
            case $ordinal >= 10 && $ordinal < 100:
                return '00' . $ordinal;
                break;
            case $ordinal >= 100 && $ordinal < 1000:
                return '0' . $ordinal;
                break;
            default:
                return $ordinal;
        }
    }

    public function show(Request $request, $id): Renderable
    {
        $currentYear   = (int)date('Y');
        $percent       = Variables::where('key', 'percentOfDeviation')->first();
        $currentReport = null;
        $month         = date("n") - 1;

        if (null !== $id) {
            $currentReport = Template::all()->where('company_id', $id)->first();
        }

        $months = Months::get();

        unset($months[12], $months[13], $months[14], $months[15]);

        $currentMonth = Months::get()[$month];
        $cMonth       = [$month => Months::get()[$month]];
        $thisYear     = Report::all()->where('company_id', $id)->where('year', $currentYear)->first();
        $params       = Param::all()->whereIn('id', $currentReport->params)->sortBy('paragraph')->keyBy('id')->toArray();

        $aggregator = new PlanAggregator(
            $lastYear->data ?? null,
            $thisYear->data ?? null,
            $nextYear->data ?? null,
            $params,
            $lastYear->fact_previous ?? null,
            $thisYear->predict_current ?? null,
            true
        );

        return view(
            'report.show',
            [
                'currentYear'   => $currentYear,
                'currentReport' => $currentReport,
                'months'        => $months,
                'years'         => range(date('Y') - 2, date('Y') + 2),
                'reports'       => Report::all(),
                'aggregated'    => $aggregator->aggregate(),
                'currentMonth'  => $currentMonth,
                'cMonth'        => array_key_first($cMonth),
                'percent'       => $percent->value,
            ]
        );
    }

    public function showReportCurMonth(Request $request)
    {
        $currentYear   = (int)date('Y');
        $currentReport = null;
        $month         = [];
        $id            = $request->get('id');
        $percent       = Variables::where('key', 'percentOfDeviation')->first();

        if (null !== $id) {
            $currentReport = Template::all()->where('company_id', $id)->first();
        }

        $data = Report::where('company_id', $id)->where('year', $currentYear)->first();

        $months = Months::get();
        unset($months[12], $months[13], $months[14], $months[15]);

        $idMonth = array_search($request->get('month'), $months, true);

        $existingData = [];

        if (null !== $data) {
            $existingData = $data->data;
        }

        $month[$idMonth] = $months[$idMonth];

        return view(
            'report.correct',
            [
                'currentYear'   => $currentYear,
                'currentReport' => $currentReport,
                'months'        => $month,
                'years'         => range(date('Y') - 2, date('Y') + 2),
                'reports'       => Report::all(),
                'existingData'  => $existingData,
                'report'        => Param::getSortedParams($this->findParamForId($currentReport->params)),
                'percent'       => $percent->value,
            ]
        );
    }

    protected function findParamForId(array $ids = []): array
    {
        $params          = Param::whereIn('id', $ids)->get();
        $treatmentParams = [];
        foreach ($params as $item) {
            $treatmentParams[] = $item;
        }
        return $treatmentParams;
    }

    public function create(Request $request)
    {
        $id          = $request->get('id');
        $currentYear = (int)date('Y');

        $report     = Template::all()->where('company_id', $id)->first();
        $dataReport = Report::all()->where('company_id', $id)->where('year', $currentYear)->first();

        if ($report->params === null) {
            return redirect('template/' . $report->id . '/edit')->with('error', 'Заполните параметры для шаблона ' . $report->company->company_name);
        }

        $params       = Param::all()->whereIn('id', array_keys($report->params))->keyBy('id')->toArray();
        $months       = Months::get();
        $currentMonth = date("n") - 1;

        unset($months[12], $months[13], $months[14], $months[15]);

        $factMonths      = $this->getCurrentMonthAndPrevious($currentMonth, $months);
        $prognosisMonths = $this->getCurrentMonthAndNext($currentMonth, $months);

        $existingData = [];

        if (null !== $dataReport) {
            $existingData = $dataReport->data;
        }

        return view(
            'report.create',
            [
                'months'         => $months,
                'report'         => Param::getSortedParams($this->findParamForId($report->params)),
                'existingData'   => $existingData,
                'params'         => $params,
                'data'           => $dataReport,
                'currentMonth'   => $months[$currentMonth],
                'factMonth'      => $factMonths,
                'prognosisMonth' => $prognosisMonths,
            ]
        );
    }

    private function getCurrentMonthAndPrevious(string $currentMonth, array $months = []): array
    {
        $firstMonthOfYear = array_key_first($months);

        $data = [];
        foreach (range($firstMonthOfYear, $currentMonth) as $value) {
            $data[$value] = $months[$value];
        }
        unset($data[$currentMonth]);

        return $data;
    }

    private function getCurrentMonthAndNext(string $currentMonth, array $months = []): array
    {
        $lastMonthOfYear = array_key_last($months);

        $data = [];
        foreach (range($currentMonth, $lastMonthOfYear) as $value) {
            $data[$value] = $months[$value];
        }
        unset($data[$currentMonth]);

        return $data;
    }

    public function showReportForCorrect(Request $request)
    {
        $id          = $request->get('id');
        $currentYear = (int)date('Y');

        $report     = Template::all()->where('company_id', $id)->first();
        $dataReport = Report::all()->where('company_id', $id)->where('year', $currentYear)->first();
        $months     = Months::get();
        $percent    = Variables::where('key', 'percentOfDeviation')->first();

        unset($months[12], $months[13], $months[14], $months[15]);

        if ($report->params === null) {
            return redirect('template/' . $report->id . '/edit')->with('error', 'Заполните параметры для шаблона ' . $report->company->company_name);
        }

        $params       = Param::all()->whereIn('id', array_keys($report->params))->keyBy('id')->toArray();
        $existingData = [];

        if (null !== $dataReport) {
            $existingData = $dataReport->data;
        }

        return view(
            'report.adjustment',
            [
                'months'       => $months,
                'report'       => Param::getSortedParams($this->findParamForId($report->params)),
                'existingData' => $existingData,
                'params'       => $params,
                'data'         => $dataReport,
                'percent'      => $percent->value,
            ]
        );
    }

    public function store(Request $request): Response
    {
        $currentYear = (int)date('Y');

        $data     = Report::all()->where('company_id', $request->get('company_id'))->where('year', $currentYear)->first();
        $template = Template::all()->where('company_id', $request->get('company_id'))->first();

        $planData = Data::all()->where('template_id', $template->id)->where('year', $currentYear)->first();

        if ($planData === null) {
            return redirect('report/')->with('error', 'Заполните план по предприятию ' . $template->company->company_name);
        }

        if (null === $data) {
            $newData = $this->addPlanValueForData($request->get('data'), $planData->data);

            $data = new Report(
                [
                    'company_id' => $request->get('company_id'),
                    'data'       => $newData,
                    'year'       => $currentYear,
                ]
            );
            $data->save();
            return redirect('/report')->with('success', 'Отчёт успешно создан!');
        }

        $newData = $this->compareData($request->get('data'), $data->data, $planData->data);

        $data->data = $newData;
        $data->save();

        return redirect('/report')->with('success', 'Данные успешно сохранены!');
    }

    private function addPlanValueForData(array $requestData, array $planData)
    {
        $newData = [];

        foreach (array_keys($requestData) as $k) {
            $treatedData = $this->treatmentDataItemForPlan($requestData[$k], $planData[$k]);
            $newData[$k] = $treatedData;
        }
        return $newData;
    }

    private function treatmentDataItemForPlan(array &$data, array $planData = []): array
    {
        $treatmentDataItem = [];
        foreach (array_keys($data) as $key) {
            $newData = [
                'plan'      => $this->getPlan($data, $planData, $key),
                'fact'      => $this->getFact($data, $data, $key),
                'prognosis' => $this->getPrognosis($data, $data, $key),
                'comment'   => $this->getComment($data, $key),
            ];

            $treatmentDataItem[] = $newData;
        }

        return $treatmentDataItem;
    }

    private function getPlan(array &$data, array $newData, int $monthId): ?float
    {
        return $newData[$monthId]['plan'] ?? ($data[$monthId]['plan'] ?? null);
    }

    private function getFact(array &$data, array $newData, int $monthId): ?float
    {
        return $newData[$monthId]['fact'] ?? ($data[$monthId]['fact'] ?? null);
    }

    private function getPrognosis(array &$data, array $newData, int $monthId): ?float
    {
        return $newData[$monthId]['prognosis'] ?? ($data[$monthId]['prognosis'] ?? null);
    }

    private function getComment(array &$data, int $monthId): ?string
    {
        return $data[$monthId]['comment'] ?? null;
    }

    private function compareData(array $requestData, array $data = [], array $planData = []): array
    {
        $newData = [];

        foreach (array_keys($requestData) as $k) {
            $treatedData = $this->treatmentDataItem($data[$k], $requestData[$k], $planData[$k]);
            $newData[$k] = $treatedData;
        }
        return $newData;
    }

    private function treatmentDataItem(array &$data, array $requestData, array $planData = []): array
    {
        $treatmentDataItem = [];
        foreach (array_keys($requestData) as $key) {
            $newData = [
                'plan'               => $this->getPlan($data, $planData, $key),
                'prognosis'          => $this->getPrognosis($data, $requestData, $key),
                'fact'               => $this->getFact($data, $requestData, $key),
                'comment'            => $this->getComment($data, $key),
                'comment_deviations' => $this->getCommentDeviation($data, $requestData, $key),
            ];

            $treatmentDataItem[] = $newData;
        }

        return $treatmentDataItem;
    }

    private function getCommentDeviation(array &$data, array &$newData, int $monthId): ?string
    {
        return $newData[$monthId]['comment_deviations'] ?? ($data[$monthId]['comment_deviations'] ?? null);
    }

    public function edit($id)
    {
        $report           = $this->treatmentReports(Report::all()->where('id', $id)->first());
        $reportCheckedIds = [];
        foreach ($report->params as $param) {
            $reportCheckedIds[] = $param['id'];
        }

        $params = Param::getAllSortedByParagraph();

        return view('report.edit', compact('report', 'params', 'reportCheckedIds'));
    }

    public function update(Request $request, $id)
    {
        $report = Report::all()->where('id', $id)->first();

        $report->params = $request->get('params');
        $report->save();

        return redirect('/report')->with('success', 'Шаблон отчёта обновлен!');
    }

    public function destroy($id)
    {
        $report = Report::all()->where('id', $id)->first();
        $report->delete();

        return redirect('/report')->with('success', 'Шаблон отчёта удален!');
    }
}
