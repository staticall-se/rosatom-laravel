<?php

namespace App\Http\Controllers;

use App\Models\Param;
use App\Models\Variables;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;


class ParamController extends Controller
{
    public function index()
    {
        $params = Param::getAllSortedByParagraph();

        return view('param.index', compact('params'));
    }

    public function create()
    {
        return view('param.create');
    }

    public function percentOfDeviation()
    {
        $percent = Variables::all()->where('key', 'percentOfDeviation')->first();

        return view(
            'param.showPercent',
            [
                'percent' => $percent->value,
            ]
        );
    }

    public function changePercentOfDeviation()
    {
        $percent = Variables::all()->where('key', 'percentOfDeviation')->first();

        return view(
            'param.changePercentOfDeviation',
            [
                'percent' => $percent->value,
            ]
        );
    }

    public function updatePercentOfDeviation(Request $request): Response
    {
        $percent        = Variables::where('key', 'percentOfDeviation')->first();
        $percent->value = (int)$request->get('percent');
        $percent->save();

        return redirect('/param')->with('success', 'Коэфицент отклонения обновлён!');
    }

    public function store(Request $request): Response
    {
        $param = new Param(
            [
                'paragraph'     => $request->get('paragraph'),
                'name'          => $request->get('name'),
                'unit'          => $request->get('unit'),
                'type_counting' => $request->get('type_counting'),
                'show_on_plan'  => $request->get('show_on_plan'),
            ]
        );
        $param->save();
        return redirect('/param')->with('success', 'Показатель сохранен!');
    }

    public function edit($id)
    {
        $param = Param::all()->where('id', $id)->first();
        return view('param.edit', compact('param'));
    }

    public function update(Request $request, $id)
    {
        $param                = Param::all()->where('id', $id)->first();
        $param->paragraph     = $request->get('paragraph');
        $param->name          = $request->get('name');
        $param->unit          = $request->get('unit');
        $param->type_counting = $request->get('type_counting');
        $param->show_on_plan  = $request->get('show_on_plan');
        $param->save();

        return redirect('/param')->with('success', 'Показатель обновлен!');
    }

    public function destroy($id)
    {
        $param = Param::all()->where('id', $id)->first();
        $param->delete();

        return redirect('/param')->with('success', 'Показатель удален!');
    }
}
