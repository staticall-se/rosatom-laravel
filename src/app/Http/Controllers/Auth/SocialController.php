<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\SocialAccount;
use App\Models\User;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{

    public function loginRedirect()
    {
        $socialiteUser = Socialite::driver('keycloak')->user();

        $user = $this->findOrCreateUser('keycloak', $socialiteUser);

        auth()->login($user, true);

        return redirect('/');
    }

    public function findOrCreateUser(string $provider, object $socialiteUser): object
    {
        if ($user = $this->findUserBySocialId($provider, $socialiteUser->id)) {
            return $user;
        }

        if ($user = $this->findUserByEmail($socialiteUser->email)) {
            $this->addSocialAccount($provider, $user, $socialiteUser);

            return $user;
        }

        $user = new User(
            [
                'name'     => $socialiteUser->name,
                'email'    => $socialiteUser->email,
                'password' => bcrypt(Str::random(25)),
            ]
        );
        $user->save();

        $this->addSocialAccount($provider, $user, $socialiteUser);

        return $user;
    }

    private function findUserBySocialId(string $provider, string $id)
    {
        $socialAccount = SocialAccount::where('provider', $provider)
            ->where('provider_id', $id)
            ->first();

        return $socialAccount->user ?? false;
    }

    public function findUserByEmail(string $email)
    {
        return !$email ? null : User::where('email', $email)->first();
    }

    public function addSocialAccount(string $provider, object $user, object $socialiteUser)
    {
        $socialAccount = new SocialAccount(
            [
                'user_id'       => $user->id,
                'provider'      => $provider,
                'provider_id'   => $socialiteUser->id,
                'token'         => $socialiteUser->token,
                'refresh_token' => $socialiteUser->refreshToken,
                'user_data'     => $socialiteUser->user
            ]
        );
        $socialAccount->save();
    }
}
