<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Param;
use App\Models\Template;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CompanyController extends Controller
{
    public function index()
    {
        $companies = Company::all();

        return view('company.index', compact('companies'));
    }

    public function create()
    {
        return view('company.create');
    }

    public function store(Request $request): Response
    {

        $company = new Company(
            [
                'company_name'                => $request->get('company_name'),
                'company_description'         => $request->get('company_description'),
                'post_of_chief'               => $request->get('post_of_chief'),
                'fio_of_chief'                => $request->get('fio_of_chief'),
                'post_of_contracting_officer' => $request->get('post_of_contracting_officer'),
                'fio_of_contracting_officer'  => $request->get('fio_of_contracting_officer'),
                'start_duration_contract'     => date('Y-m-d', strtotime($request->get('start_duration_contract') ?? 'now')),
                'finish_duration_contract'    => date('Y-m-d', strtotime($request->get('finish_duration_contract') ?? 'now')),
                'location_address'            => $request->get('location_address'),
                'telephone'                   => $request->get('telephone'),
                'fax'                         => $request->get('fax'),
                'email'                       => $request->get('email'),
                'industry'                    => $request->get('industry'),
                'main_activity'               => $request->get('main_activity'),
                'amount_statutory_fund'       => $request->get('amount_statutory_fund'),
                'post_of_executive_director'  => $request->get('post_of_executive_director'),
                'fio_perpetrator'             => $request->get('fio_perpetrator'),
                'mobile_perpetrator'          => $request->get('mobile_perpetrator'),
                'email_perpetrator'           => $request->get('email_perpetrator'),
                'balance_board_review_date'   => date('Y-m-d H:i:s', strtotime('now')),
            ]
        );

        $company->save();

        $report  = new Template(
            [
                'company_id' => $company->id,
                'params'     => null,
            ]
        );
        $report->save();

        return redirect('/company')->with('success', 'Муниципалитет добавлен!');
    }

    public function edit($id)
    {
        $company = Company::all()->where('id', $id)->first();
        return view('company.edit', compact('company'));
    }

    public function show($id)
    {
        $company = Company::all()->where('id', $id)->first();

        return view('company.show', compact('company'));
    }

    public function update(Request $request, $id)
    {

        $company                              = Company::all()->where('id', $id)->first();
        $company->company_name                = $request->get('company_name');
        $company->company_description         = $request->get('company_description');
        $company->post_of_chief               = $request->get('post_of_chief');
        $company->fio_of_chief                = $request->get('fio_of_chief');
        $company->post_of_contracting_officer = $request->get('post_of_contracting_officer');
        $company->fio_of_contracting_officer  = $request->get('fio_of_contracting_officer');
        $company->start_duration_contract     = date('Y-m-d', strtotime($request->get('start_duration_contract')));
        $company->finish_duration_contract    = date('Y-m-d', strtotime($request->get('finish_duration_contract')));
        $company->location_address            = $request->get('location_address');
        $company->telephone                   = $request->get('telephone');
        $company->fax                         = $request->get('fax');
        $company->email                       = $request->get('email');
        $company->industry                    = $request->get('industry');
        $company->main_activity               = $request->get('main_activity');
        $company->amount_statutory_fund       = $request->get('amount_statutory_fund');
        $company->post_of_executive_director  = $request->get('post_of_executive_director');
        $company->fio_perpetrator             = $request->get('fio_perpetrator');
        $company->mobile_perpetrator          = $request->get('mobile_perpetrator');
        $company->email_perpetrator           = $request->get('email_perpetrator');
        $company->balance_board_review_date   = date('Y-m-d H:i:s', strtotime('now'));

        $company->save();

        return redirect('/company')->with('success', 'Муниципалитет обновлен!');
    }

    public function destroy($id)
    {
        $company = Company::all()->where('id', $id)->first();
        $company->delete();

        return redirect('/company')->with('success', 'Муниципалитет удален!');
    }

    public function assignParams(): Renderable
    {
        $companies = Company::all();
        $params    = Param::all();
        return view('company', compact('companies', 'params'));
    }
}
