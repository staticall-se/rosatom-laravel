<?php declare(strict_types=1);

namespace App;

interface AggregatorInterface
{
    public function aggregate();
}
