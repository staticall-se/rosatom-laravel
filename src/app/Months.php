<?php

namespace App;

class Months
{
    public const QUARTER_1    = 12;
    public const JANUARY_ID   = 0;
    public const FEBRUARY_ID  = 1;
    public const MARCH_ID     = 2;
    public const QUARTER_2    = 13;
    public const APRIL_ID     = 3;
    public const MAY_ID       = 4;
    public const JUNE_ID      = 5;
    public const QUARTER_3    = 14;
    public const JULY_ID      = 6;
    public const AUGUST_ID    = 7;
    public const SEPTEMBER_ID = 8;
    public const QUARTER_4    = 15;
    public const OCTOBER_ID   = 9;
    public const NOVEMBER_ID  = 10;
    public const DECEMBER_ID  = 11;

    public static function get(): array
    {
        return [
            self::QUARTER_1    => 'I квартал',
            self::JANUARY_ID   => 'Январь',
            self::FEBRUARY_ID  => 'Февраль',
            self::MARCH_ID     => 'Март',
            self::QUARTER_2    => 'II квартал',
            self::APRIL_ID     => 'Апрель',
            self::MAY_ID       => 'Май',
            self::JUNE_ID      => 'Июнь',
            self::QUARTER_3    => 'III квартал',
            self::JULY_ID      => 'Июль',
            self::AUGUST_ID    => 'Август',
            self::SEPTEMBER_ID => 'Сентябрь',
            self::QUARTER_4    => 'IV квартал',
            self::OCTOBER_ID   => 'Октябрь',
            self::NOVEMBER_ID  => 'Ноябрь',
            self::DECEMBER_ID  => 'Декабрь',
        ];
    }
}
