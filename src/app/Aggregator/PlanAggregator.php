<?php declare(strict_types=1);

namespace App\Aggregator;

use App\AggregatorInterface;
use App\Dto\PlanResultCollectionDto;
use App\Dto\PlanResultDto;

final class PlanAggregator implements AggregatorInterface
{
    private $lastYear;
    private $currentYear;
    private $nextYear;
    private $params;
    private $factPrevious;
    private $predictCurrent;
    private $isTemplate;

    public function __construct(?array $lastYear, ?array $currentYear, ?array $nextYear, array $params, ?array $factPrevious, ?array $predictCurrent, bool $isTemplate = false)
    {
        $this->lastYear = $lastYear;
        $this->currentYear = $currentYear;
        $this->nextYear = $nextYear;
        $this->params = $params;
        $this->factPrevious = $factPrevious;
        $this->predictCurrent = $predictCurrent;
        $this->isTemplate = $isTemplate;
    }

    public function aggregate(): PlanResultCollectionDto
    {
        $collectionDto = new PlanResultCollectionDto();

        foreach ($this->params as $paramId => $param) {
            $dto = new PlanResultDto(
                $param,
                empty($this->lastYear[$paramId]) ? [] : $this->lastYear[$paramId],
                empty($this->currentYear[$paramId]) ? [] : $this->currentYear[$paramId],
                empty($this->nextYear[$paramId]) ? [] : $this->nextYear[$paramId],
                empty($this->factPrevious) ? [] : $this->factPrevious,
                empty($this->predictCurrent) ? [] : $this->predictCurrent,
                $this->isTemplate
            );

            $collectionDto->add($dto);
        }

        return $collectionDto;
    }
}
