<?php declare(strict_types=1);

namespace App\Dto;

final class PlanResultCollectionDto
{
    /** @var PlanResultDto[] */
    private $items;

    public function __construct()
    {
        $this->items = [];
    }

    public function add(PlanResultDto $dto): bool
    {
        if (true === in_array($dto, $this->items, true)) {
            return false;
        }

        $this->items[] = $dto;

        return true;
    }

    /**
     * @return PlanResultDto[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
