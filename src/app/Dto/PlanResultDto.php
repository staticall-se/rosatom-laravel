<?php declare(strict_types=1);

namespace App\Dto;

use App\Months;

final class PlanResultDto
{
    private const TYPE_PLAN          = 'plan';
    private const TYPE_FACT          = 'fact';
    private const TYPE_PROGNOSIS     = 'prognosis';
    private const TYPE_PREDICT       = 'predict_current';
    private const TYPE_FACT_PREVIOUS = 'fact_previous';
    private const TYPE_COUNTING_SUM  = 'sum';

    public const QUARTER_1 = 12;
    public const QUARTER_2 = 13;
    public const QUARTER_3 = 14;
    public const QUARTER_4 = 15;

    private $id;
    private $paragraph;
    private $name;
    private $unit;
    private $factPrevious;
    private $predictCurrent;
    private $type_counting;

    private $percentsOfDeviation = 0;

    private $planOnYear            = [];
    private $factOnYear            = [];
    private $prognosisOnYear            = [];
    private $commentsPlan          = [];
    private $commentsDeviationPlan = [];

    private $planQ1 = 0;
    private $planQ2 = 0;
    private $planQ3 = 0;
    private $planQ4 = 0;

    private $totalPlanFutureYear = 0;

    private $predictCurrentYear = 0;
    private $factPreviousYear   = 0;

    private $prognosisOnCurrentYear               = 0;
    private $deviationFactFromPlanSum             = 0;
    private $deviationFactFromPlanPercent         = 0;
    private $deviationFactFromCurrentMonthSum     = 0;
    private $deviationFactFromCurrentMonthPercent = 0;

    public function __construct(array $parameter, array $lastYear, array $currentYear, array $nextYear, ?array $factPrevious, ?array $predictCurrent, bool $isTemplate = false)
    {
        $this->id             = $parameter['id'];
        $this->paragraph      = $parameter['paragraph'];
        $this->name           = $parameter['name'];
        $this->unit           = $parameter['unit'];
        $this->type_counting  = $parameter['type_counting'];
        $this->show_on_plan   = $parameter['show_on_plan'];
        $this->factPrevious   = $factPrevious;
        $this->predictCurrent = $predictCurrent;


        foreach (Months::get() as $monthId => $month) {
            $this->planOnYear[$monthId]            = $this->getValue($currentYear, $monthId);
            $this->factOnYear[$monthId]            = !empty($currentYear[$monthId]['fact']) ? $currentYear[$monthId]['fact'] : null;
            $this->prognosisOnYear[$monthId]            = !empty($currentYear[$monthId]['prognosis']) ? $currentYear[$monthId]['prognosis'] : null;
            $this->commentsPlan[$monthId]          = !empty($currentYear[$monthId]['comment']) ? $currentYear[$monthId]['comment'] : null;
            $this->commentsDeviationPlan[$monthId] = !empty($currentYear[$monthId]['comment_deviations']) ? $currentYear[$monthId]['comment_deviations'] : null;
        }

        $this->planQ1 = $this->calculateQ1($currentYear, $this->type_counting);
        $this->planQ2 = $this->calculateQ2($currentYear, $this->type_counting);
        $this->planQ3 = $this->calculateQ3($currentYear, $this->type_counting);
        $this->planQ4 = $this->calculateQ4($currentYear, $this->type_counting);

        $this->planOnYear = $this->addQuarters($this->planOnYear);

        $this->totalPlanFutureYear = $this->calculateForYearQuarter($this->type_counting);

        $this->predictCurrentYear = $this->getValueTotalFactOrPredict($this->predictCurrent, $this->id, self::TYPE_PREDICT);
        $this->factPreviousYear   = $this->getValueTotalFactOrPredict($this->factPrevious, $this->id, self::TYPE_FACT_PREVIOUS);

        $this->percentsOfDeviation = $this->processDeviationPercents($this->predictCurrentYear, $this->factPreviousYear);

        if ($isTemplate === true) {
            $this->prognosisOnCurrentYear               = $this->calculateForYearQuarterForTemplate($currentYear, $this->type_counting);
            $this->deviationFactFromPlanSum             = $this->countDeviationFromPlanSum($currentYear);
            $this->deviationFactFromPlanPercent         = $this->countDeviationFromPlanPercent($currentYear);
            $this->deviationFactFromCurrentMonthSum     = $this->countDeviationFromCurrentMonthSum($currentYear);
            $this->deviationFactFromCurrentMonthPercent = $this->countDeviationFromCurrentMonthPercent($currentYear);
        }
    }

    private function getValue(array $yearData, int $monthId, string $type = self::TYPE_PLAN): float
    {
        return !empty($yearData[$monthId]) ? (float)$yearData[$monthId][$type] : 0;
    }

    private function calculateQ1(array $yearData, string $type_counting, string $type = self::TYPE_PLAN): float
    {
        $values = [
            $this->getValue($yearData, Months::JANUARY_ID, $type),
            $this->getValue($yearData, Months::FEBRUARY_ID, $type),
            $this->getValue($yearData, Months::MARCH_ID, $type),
        ];

        if ($type_counting === self::TYPE_COUNTING_SUM) {
            $value = array_sum($values);
            return (float)number_format($value, 2, '.', '');
        }

        $value = array_sum($values) / 3;
        return (float)number_format($value, 2, '.', '');
    }

    private function calculateQ2(array $yearData, string $type_counting, string $type = self::TYPE_PLAN): float
    {
        $values = [
            $this->getValue($yearData, Months::APRIL_ID, $type),
            $this->getValue($yearData, Months::MAY_ID, $type),
            $this->getValue($yearData, Months::JUNE_ID, $type),
        ];

        if ($type_counting === self::TYPE_COUNTING_SUM) {
            $value = array_sum($values);
            return (float)number_format($value, 2, '.', '');
        }

        $value = array_sum($values) / 3;
        return (float)number_format($value, 2, '.', '');
    }

    private function calculateQ3(array $yearData, string $type_counting, string $type = self::TYPE_PLAN): float
    {
        $values = [
            $this->getValue($yearData, Months::JULY_ID, $type),
            $this->getValue($yearData, Months::AUGUST_ID, $type),
            $this->getValue($yearData, Months::SEPTEMBER_ID, $type),
        ];

        if ($type_counting === self::TYPE_COUNTING_SUM) {
            $value = array_sum($values);
            return (float)number_format($value, 2, '.', '');
        }

        $value = array_sum($values) / 3;
        return (float)number_format($value, 2, '.', '');
    }

    private function calculateQ4(array $yearData, string $type_counting, string $type = self::TYPE_PLAN): float
    {
        $values = [
            $this->getValue($yearData, Months::OCTOBER_ID, $type),
            $this->getValue($yearData, Months::NOVEMBER_ID, $type),
            $this->getValue($yearData, Months::DECEMBER_ID, $type),
        ];

        if ($type_counting === self::TYPE_COUNTING_SUM) {
            $value = array_sum($values);
            return (float)number_format($value, 2, '.', '');
        }

        $value = array_sum($values) / 3;
        return (float)number_format($value, 2, '.', '');
    }

    public function addQuarters(array $plan, string $type = self::TYPE_PLAN): array
    {
        if ($type === self::TYPE_PLAN) {
            $plan[self::QUARTER_1] = $this->planQ1;
            $plan[self::QUARTER_2] = $this->planQ2;
            $plan[self::QUARTER_3] = $this->planQ3;
            $plan[self::QUARTER_4] = $this->planQ4;
        }
        return $plan;
    }

    private function calculateForYearQuarter(string $type_counting): float
    {
        $values = [
            $this->planQ1,
            $this->planQ2,
            $this->planQ3,
            $this->planQ4,
        ];
        if ($type_counting === self::TYPE_COUNTING_SUM) {
            $value = array_sum($values);
            return (float)number_format($value, 2, '.', '');
        }

        $value = array_sum($values) / 4;
        return (float)number_format($value, 2, '.', '');
    }

    private function getValueTotalFactOrPredict(array $yearData, string $paramId, string $type): float
    {
        return !empty($yearData[$paramId]) ? (float)$yearData[$paramId][$type] : 0;
    }

    private function processDeviationPercents(float $futureYear, float $currentYear): float
    {
        if ($currentYear > 0) {
            return ($futureYear / $currentYear) * 100 - 100;
        }
        return 0;
    }

    private function calculateForYearQuarterForTemplate(array $yearData, string $type_counting): float
    {
        $getCurrentMonthAndPrevious = $this->getCurrentMonthAndPrevious();
        $values = [];
        $months = Months::get();
        unset($months[12], $months[13], $months[14], $months[15]);

        foreach ($getCurrentMonthAndPrevious as $value) {
            foreach ($months as $keyMonth => $month) {
                if ($month === $value) {
                    $values[] = $this->getValue($yearData, $keyMonth, self::TYPE_FACT);
                } else {
                    $values[] = $this->getValue($yearData, $keyMonth, self::TYPE_PROGNOSIS);
                }
            }
        }

        if ($type_counting === self::TYPE_COUNTING_SUM) {
            $value = array_sum($values);
            return (float)number_format($value, 2, '.', '');
        }

        $value = array_sum($values) / 4;
        return (float)number_format($value, 2, '.', '');
    }

    private function getCurrentMonthAndPrevious(): array
    {
        $currentMonth = date("n") - 1;
        $months       = Months::get();
        unset($months[12], $months[13], $months[14], $months[15]);
        $firstMonthOfYear = key($months);

        $data = [];
        foreach (range($firstMonthOfYear, $currentMonth) as $value) {
            $data[$value] = $months[$value];
        }

        return $data;
    }

    private function countDeviationFromPlanSum(array $yearData, string $type = self::TYPE_PLAN)
    {
        $plan = $this->getValue($yearData, $this->getCurrentMonth(), $type);
        $fact = $this->getFact($yearData, $this->getCurrentMonth());

        return $fact - $plan;
    }

    private function getCurrentMonth()
    {
        return date("n") - 1;
    }

    private function getFact(array $yearData, int $monthId, string $type = self::TYPE_FACT): float
    {
        return $yearData[$monthId][$type] ?? 0;
    }

    private function countDeviationFromPlanPercent(array $yearData, string $type = self::TYPE_PLAN)
    {
        $plan = $this->getValue($yearData, $this->getCurrentMonth(), $type);
        $fact = $this->getFact($yearData, $this->getCurrentMonth());

        if ($plan > 0 && $fact > 0) {
            return ($fact / $plan * 100) - 100;
        }
        return 0;
    }

    private function countDeviationFromCurrentMonthSum(array $yearData, string $type = self::TYPE_PLAN): float
    {
        $data = $this->getValuesPlanAndFact($yearData, $type);

        if (count($data) < 1) {
            return 0;
        }

        if (is_float($data[0]) && is_float($data[1])) {
            return $data[0] - $data[1];
        }

        return array_sum($data[0]) - array_sum($data[1]);
    }

    private function getValuesPlanAndFact(array $yearData, string $type = self::TYPE_PLAN): ?array
    {
        $pullMonths = $this->getCurrentMonthAndPrevious();
        $valuesPlan = [];
        $valuesFact = [];

        if (count($pullMonths) <= 1) {
            $fact = $this->getFact($yearData, $this->getCurrentMonth());
            $plan = $this->getValue($yearData, $this->getCurrentMonth(), $type);

            if ($fact > 0) {
                return [
                    $fact,
                    $plan,
                ];
            }
            return [];
        }

        foreach ($pullMonths as $key => $value) {
            $fact             = $this->getFact($yearData, $key);
            $plan             = $this->getValue($yearData, $key, $type);
            $valuesPlan[]     = $plan;
            $valuesFact[$key] = $fact;
        }

        return [
            $valuesPlan,
            $valuesFact,
        ];
    }

    private function countDeviationFromCurrentMonthPercent(array $yearData, string $type = self::TYPE_PLAN)
    {
        $data = $this->getValuesPlanAndFact($yearData, $type);

        if (count($data) < 1) {
            return 0;
        }

        if (is_float($data[0]) && is_float($data[1])) {
            return (($data[0] / $data[1]) * 100) - 100;
        }

        if (array_sum($data[1]) > 0) {
            return (array_sum($data[0]) / array_sum($data[1]) * 100) - 100;
        }
        return 0;
    }

    public function getParagraph(): string
    {
        return $this->paragraph;
    }

    public function getPrognosisOnCurrentYear(): ?float
    {
        return (float)number_format($this->prognosisOnCurrentYear, 2, '.', '');
    }

    public function getDeviationFactFromPlanSum(): ?float
    {
        return (float)number_format($this->deviationFactFromPlanSum, 2, '.', '');
    }

    public function getDeviationFromPlanPercent(): ?float
    {
        return (float)number_format($this->deviationFactFromPlanPercent, 2, '.', '');
    }

    public function getDeviationFactFromCurrentMonthSum(): ?float
    {
        return (float)number_format($this->deviationFactFromCurrentMonthSum, 2, '.', '');
    }

    public function getDeviationFactFromCurrentMonthPercent(): ?float
    {
        return (float)number_format($this->deviationFactFromCurrentMonthPercent, 2, '.', '');
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUnit(): string
    {
        return $this->unit;
    }

    public function getTypeShowOnPlan(): string
    {
        return $this->show_on_plan;
    }

    public function getTotalFactLastYear(): ?float
    {
        return $this->factPreviousYear;
    }

    public function getTotalPlanCurrentYear(): float
    {
        return $this->predictCurrentYear;
    }

    public function getTotalPlanFutureYear(): float
    {
        return $this->totalPlanFutureYear;
    }

    public function getPercentsDeviation(): float
    {
        return $this->percentsOfDeviation;
    }

    public function getPlanOnYearFor(int $monthId): float
    {
        return $this->planOnYear[$monthId];
    }

    public function getFactOnYearFor(int $monthId): ?float
    {
        return $this->factOnYear[$monthId];
    }

    public function getPrognosisOnYearFor(int $monthId): ?float
    {
        return $this->prognosisOnYear[$monthId];
    }

    public function getCommentForYear(int $monthId): ?string
    {
        return $this->commentsPlan[$monthId];
    }

    public function getCommentDeviation(int $monthId): ?string
    {
        return $this->commentsDeviationPlan[$monthId];
    }

    private function getCurrentMonthAndNext(string $currentMonth, array $months = []): array
    {
        $lastMonthOfYear = array_key_last($months);

        $data = [];
        foreach (range($currentMonth, $lastMonthOfYear) as $value) {
            $data[$value] = $months[$value];
        }
        unset($data[$currentMonth]);

        return $data;
    }
}
