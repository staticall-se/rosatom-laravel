<?php declare(strict_types=1);

namespace App\Dto;

final class PlanResultLineValueDto
{
    private $month;
    private $plan;
    private $fact;
    private $comment;

    /**
     * @param string      $month
     * @param float       $plan
     * @param float       $fact
     * @param string|null $comment
     */
    public function __construct(
        string $month,
        float $plan,
        float $fact,
        ?string $comment
    )
    {
        $this->month = $month;
        $this->plan = $plan;
        $this->fact = $fact;
        $this->comment = $comment;
    }

    public function getMonth(): string
    {
       return $this->month;
    }

    public function getPlan(): float
    {
        return $this->plan;
    }

    public function getFact(): float
    {
        return $this->fact;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }
}
