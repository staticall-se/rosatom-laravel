<?php declare(strict_types=1);

namespace App\Dto;

final class PlanResultLineDto
{
    private $paragraph;
    private $name;
    private $unit;

    /** @var PlanResultLineValueDto[] */
    private $values;

    /**
     * @param string                   $paragraph
     * @param string                   $name
     * @param string                   $unit
     * @param PlanResultLineValueDto[] $values
     */
    public function __construct(string $paragraph, string $name, string $unit, array $values)
    {
        $this->paragraph = $paragraph;
        $this->name = $name;
        $this->unit = $unit;
        $this->values = $values;
    }

    public function getParagraph(): string
    {
        return $this->paragraph;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUnit(): string
    {
        return $this->unit;
    }

    public function getValues(): array
    {
        return $this->values;
    }
}
