<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'template_id',
        'data',
        'year',
        'fact_previous',
        'predict_current',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'data'            => 'array',
        'fact_previous'   => 'array',
        'predict_current' => 'array',
    ];


}
