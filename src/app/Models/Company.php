<?php

namespace App\Models;

class Company extends BaseModel
{
    protected $fillable = [
        'id',
        'company_name',
        'company_description',
        'post_of_chief',
        'fio_of_chief',
        'post_of_contracting_officer',
        'fio_of_contracting_officer',
        'start_duration_contract',
        'finish_duration_contract',
        'location_address',
        'telephone',
        'fax',
        'email',
        'industry',
        'main_activity',
        'amount_statutory_fund',
        'post_of_executive_director',
        'fio_perpetrator',
        'mobile_perpetrator',
        'email_perpetrator',
        'balance_board_review_date'
    ];

    protected $casts    = [
        'start_duration_contract'   => 'timestamp',
        'finish_duration_contract'  => 'timestamp',
        'balance_board_review_date' => 'timestamp',
    ];
}
