<?php

namespace App\Models;

class Variables extends BaseModel
{
    protected $fillable = [
        'id',
        'key',
        'value',
    ];
}
