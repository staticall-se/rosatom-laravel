<?php

namespace App\Models;

class Comment extends BaseModel
{
    protected $fillable = [
        'id',
        'data_id',
        'data',
    ];
}
