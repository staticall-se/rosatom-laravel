<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    protected $fillable = [
        'id',
        'company_id',
        'params',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'params' => 'array',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
}
