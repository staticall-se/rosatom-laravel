<?php

namespace App\Models;

use function count;

class Param extends BaseModel
{
    protected $fillable = [
        'id',
        'paragraph',
        'name',
        'unit',
        'type_counting',
        'show_on_plan',
    ];

    public static function getAllSortedByParagraph()
    {
        return self::all()->sort(
                static function ($a, $b) {
                    $aArr = explode('.', $a['paragraph']);
                    $bArr = explode('.', $b['paragraph']);
                    if (count($aArr) >= count($bArr)) {
                        $count = count($aArr);
                        for ($i = 0; $i < $count; $i ++) {
                            if (isset($bArr[$i])) {
                                if ((int)$aArr[$i] < (int)$bArr[$i]) {
                                    return - 1;
                                }
                                if ((int)$aArr[$i] > (int)$bArr[$i]) {
                                    return 1;
                                }
                            }
                        }
                    }
                    else {
                        $count = count($bArr);
                        for ($i = 0; $i < $count; $i ++) {
                            if (isset($aArr[$i])) {
                                if ((int)$bArr[$i] < (int)$aArr[$i]) {
                                    return 1;
                                }
                                if ((int)$bArr[$i] > (int)$aArr[$i]) {
                                    return - 1;
                                }
                            }
                        }
                    }
                    return 0;
                }
            );
    }

    public static function getSortedParams(array $param = [])
    {
        usort($param, static function($a, $b)
        {
            return strcmp($a->paragraph, $b->paragraph);
        });
        return $param;
    }

}
