<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = [
        'id',
        'company_id',
        'data',
        'year',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'data' => 'array'
    ];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
}
