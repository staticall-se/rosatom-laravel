@extends('adminlte::page')

@section('title', 'План')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h3>План финансово-хозяйственной деятельности предприятия по основным показателям деятельности</h3>
            <div class="row-cols-sm-5 mb-2">
                <label for="year">Отчётный год</label>
                <select type="text" class="form-control" id="year"
                        onchange="document.location.href = '{{ route('plan.index') }}?year=' + this.value">
                    @foreach($years as $year)
                        <option value="{{ $year }}"{{ $year === $currentYear ? ' selected' : '' }}>
                            {{ $year }}
                        </option>
                    @endforeach
                </select>
            </div>
            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            @if(session()->get('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
            @endif
            @if(count($reports) > 0)
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Предприятие</th>
                            <th>Статус</th>
                            <th>Дата утверждения</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($reports as $item)
                            <tr class='tr-custom'>
                                <td>{{ $item->ordinal }}</td>
                                <td class='bold'>{{ $item->company->company_name }}</td>
                                <td>
                            <span class="tag tag-entered bold">
                                Поступил
                            </span>
                                </td>
                                <td>{{ \Carbon\Carbon::parse($item->company->balance_board_review_date)->format('d.m.Y') }}</td>
                                <td class='bold'>
                                    @if(!empty($item->isCurrentYearPlan))
                                        <a href='#' class='btn btn-primary ml-2'>
                                            <i class="fas fa-print"></i>
                                            Печать в ПФХД
                                        </a>
                                    @endif
                                    @if(!empty($item->isCurrentYearPlan))
                                        <a href='{{ route('plan.show', [$item->company->id, 'year'=>$currentYear]) }}'
                                           class='btn btn-info ml-2'>
                                            Просмотр плана
                                        </a>
                                    @endif
                                    @if(empty($item->isCurrentYearPlan))
                                        <a href="{{ route('plan.step2', ['id' => $item->company->id, 'year'=> $currentYear ])}}" class="btn btn-primary ml-2">
                                            Создать план
                                        </a>
                                    @endif
                                    @if(!empty($item->isCurrentYearPlan))
                                        <a href='{{ route('plan.editPlan', ['id' => $item->company->id, 'year'=>$currentYear]) }}'
                                           class='btn btn-warning ml-2'>
                                            Корректировка плана
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
    </div>
@stop
