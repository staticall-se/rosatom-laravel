@extends('adminlte::page')

@section('title', 'Редактировать шаблон отчёта')

@section('content')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-3">Обновить шаблон отчёта</h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <br/>
            @endif
            <form method="post" action="{{ route('plan.update', $report->id) }}">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="company_id">Муниципалитет:</label>
                    <input type="text" class="form-control" id="company_id" name="company_id"
                           value="{{ $report->company_id }}" required/>
                </div>
                <div class="form-group">
                    <label for="name">Показатели:</label>
                    @foreach($report->params as $param)
                        <input type="text" class="form-control" id="params" name="params" value="{{ $param }}"
                               required/>
                    @endforeach
                </div>
                <button type="submit" class="btn btn-primary">Обновить</button>
            </form>
        </div>
    </div>
@endsection
