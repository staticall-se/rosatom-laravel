@extends('adminlte::page')

@section('title', 'Добавление даннные в таблицу')

@section('content')
    <div id="app">
        <div class="row">
            <div class="col-12">
                <form method="post" action="{{ route('plan.store') }}">
                    @csrf
                    <input type="hidden" name="report_id" value="{{ request()->get('id') }}">
                    <input type="hidden" name="year" value="{{ request()->get('year') }}">
                    <div class="card">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br/>
                        @endif
                        <div class="card-body table-responsive p-0" style="height: 100vh;">
                            <table class="table table-head-fixed">
                                <thead>
                                    <tr>
                                        <th rowspan="2">Индекс</th>
                                        <th rowspan="2">Показатель</th>
                                        <th colspan="1" rowspan="2" style='text-align: center;'>Факт предыдущего года</th>
                                        <th colspan="1" rowspan="2" style='text-align: center;'>Прогноз до конца текущего года</th>
                                        @foreach($months as $month)
                                            <th style="text-align: center;" colspan="1">{{ $month }}</th>
                                        @endforeach
                                    </tr>
                                    <tr>
                                        @foreach($months as $month)
                                            <th style="text-align: center; position: sticky; top: 48px; background-color: white;" colspan="1">
                                                план
                                            </th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($report as $item)
                                    <tr>
                                        <td>{{ $item['paragraph'] }}</td>
                                        <td>{{ $item['name'] }}</td>
                                        <td>
                                            <input style='width: 100px;' type="number" class="form-control" name='fact_previous[{{ $item['id'] }}][fact_previous]'
                                                   value='{{ (empty($data->fact_previous[$item['id']]['fact_previous']) ? null : $data->fact_previous[$item['id']]['fact_previous']) }}'>
                                        </td>
                                        <td>
                                            <input style='width: 100px;' type="number" class="form-control" name='predict_current[{{ $item['id'] }}][predict_current]'
                                                   value='{{ (empty($data->predict_current[$item['id']]['predict_current']) ? null : $data->predict_current[$item['id']]['predict_current']) }}'>
                                        </td>
                                        @foreach($months as $monthId => $month)
                                            <td>
                                                <input class="form-control" style='width: 100px;' type="number" name="data[{{ $item['id'] }}][{{ $monthId }}][plan]"
                                                       value="{{ (empty($existingData[$item['id']][$monthId]['plan']) ? null : $existingData[$item['id']][$monthId]['plan']) }}">
                                                <textarea class='form-control mt-1' style='width: 100px;' placeholder="Комментарий к плану"
                                                          name="data[{{ $item['id'] }}][{{ $monthId }}][comment]">{{ (empty($existingData[$item['id']][$monthId]['comment']) ? null : $existingData[$item['id']][$monthId]['comment']) }}</textarea>
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Добавить данные</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
