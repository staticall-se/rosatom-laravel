@extends('adminlte::page')

@section('title', 'Редактировать шаблон отчёта')

@section('content')
    <div class="row">
        <div class="col-12">
            <form method="post" action="{{ route('plan.update', request()->get('id')) }}">
                @csrf
                @method('PATCH')
                <input type="hidden" name="report_id" value="{{ request()->get('id') }}">
                <input type="hidden" name="year" value="{{ request()->get('year') }}">
                <input type="hidden" id='percent' name="percentOfDeviation" value="{{ $percent }}">

                <div class="card">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br/>
                    @endif
                    <div class="card-body table-responsive p-0" style="height: 100vh;">
                        <table class="table table-head-fixed">
                            <thead>
                            <tr>
                                <th rowspan="2">Индекс</th>
                                <th rowspan="2">Показатель</th>
                                <th colspan="2" rowspan="2" style='text-align: center;'>Факт предыдущего года</th>
                                <th colspan="2" rowspan="2" style='text-align: center;'>Прогноз до конца текущего года</th>
                                @foreach($months as $month)
                                    <th style="text-align: center;" colspan="1">{{ $month }}</th>
                                @endforeach
                            </tr>
                            <tr>
                                @foreach($months as $month)
                                    <th style="text-align: center; position: sticky; top: 48px; background-color: white;">
                                        план
                                    </th>
                                    <th style="text-align: center; position: sticky; top: 48px; background-color: white;">
                                        корректировка
                                    </th>
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($report as $item)
                                <tr>
                                    <td>{{ $item['paragraph'] }}</td>
                                    <td>{{ $item['name'] }}</td>
                                    <td>
                                        <input style='width: 100px;' type="number" class="form-control" name='fact_previous[{{ $item['id'] }}][fact_previous]'
                                               value='{{ (empty($data->fact_previous[$item['id']]['fact_previous']) ? null : $data->fact_previous[$item['id']]['fact_previous']) }}' disabled>
                                    </td>
                                    <td>
                                        <input style='width: 100px;' type="number" class="form-control" name='fact_previous[{{ $item['id'] }}][fact_previous]'>
                                    </td>
                                    <td>
                                        <input style='width: 100px;' type="number" class="form-control" name='predict_current[{{ $item['id'] }}][predict_current]'
                                               value='{{ (empty($data->predict_current[$item['id']]['predict_current']) ? null : $data->predict_current[$item['id']]['predict_current']) }}' disabled>
                                    </td>
                                    <td>
                                        <input style='width: 100px;' type="number" class="form-control" name='predict_current[{{ $item['id'] }}][predict_current]'>
                                    </td>
                                    @foreach($months as $monthId => $month)
                                        <td>
                                            <input class="form-control js-plan"
                                                   style='width: 100px; text-align: center;' type="number"
                                                   data-id='{{ $item['id'] . $monthId }}'
                                                   name="data[{{ $item['id'] }}][{{ $monthId }}][plan]"
                                                   value="{{ (empty($existingData[$item['id']][$monthId]['plan']) ? null : $existingData[$item['id']][$monthId]['plan']) }}"
                                                   disabled>
                                        </td>
                                        <td>
                                            <input class="form-control js-adjust"
                                                   style='width: 100px; text-align: center;' type="number"
                                                   name="data[{{ $item['id'] }}][{{ $monthId }}][plan]"
                                                   data-id='{{ $item['id'] . $monthId }}'>
                                            <textarea class='form-control mt-1' style='display: none; width: 100px;'
                                                      placeholder="Комментарий к плану"
                                                      data-id='{{ $item['id'] . $monthId }}'
                                                      name="data[{{ $item['id'] }}][{{ $monthId }}][comment_deviations]">{{ (empty($existingData[$item['id']][$monthId]['comment_deviations']) ? null : $existingData[$item['id']][$monthId]['comment_deviations']) }}</textarea>
                                        </td>
                                    @endforeach
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Скорректировать данные</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('page-js-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.js-adjust').on('change', function () {
                var $adjust = $(this);
                var $plan = $adjust.closest('tr').find('.js-plan[data-id="' + $adjust.data('id') + '"]');
                var $comment = $adjust.closest('tr').find('textarea[data-id="' + $adjust.data('id') + '"]');
                var getPercent = $('#percent').attr('value');

                if (isNaN(parseFloat($adjust.val())) || isNaN(parseFloat($plan.val()))) {
                    return;
                }
                var percent = parseFloat($adjust.val()) * 100 / parseFloat($plan.val()) - 100;

                var maxPercentPositive = getPercent;
                var maxPercentNegative = -getPercent;

                if (percent >= maxPercentPositive && percent !== 0) {
                    return $comment['attr']('required', true) && $comment['show']();
                } else if (percent <= maxPercentNegative && percent !== 0) {
                    return $comment['attr']('required', true) && $comment['show']();
                } else {
                    return $comment['removeAttr']('required', true) && $comment['hide']();
                }
            })
        });
    </script>
@stop
