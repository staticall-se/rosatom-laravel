@extends('adminlte::page')

@section('title', 'Детальная информация')

@section('content_header')
    <h4>План финансово-хозяйственной деятельности предприятия {{ $currentReport->company->company_name }} по основным
        показателям
        деятельности за {{ $currentYear }}</h4>
    <a class='btn btn-primary mt-2' href='{{ route('plan.index') }}'>Вернуться</a>
@stop
<style>
    .content-wrapper {
        overflow: auto;
    }
</style>

@section('content')
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td rowspan="2">
                Индекс
            </td>
            <td rowspan="2">
               Показатели
            </td>
            <td rowspan="2">
                Единица<br>измерения
            </td>
            <td rowspan="2">
                Факт<br>предыдущего {{ $currentYear - 1 }} года
            </td>
            <td rowspan="2">
                Прогноз до конца текущего {{ $currentYear }} года
            </td>
            <td colspan="17">
                План на {{ $currentYear }} год
            </td>
            <td colspan="2">
                Отклонение плана {{ $currentYear }} <br/>
                от прогноза {{ $currentYear - 1 }}
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">
                План на {{ $currentYear }} год
            </td>
            @foreach($months as $monthId => $month)
                <td style="text-align: center;">
                    {{ $month }}
                </td>
            @endforeach
            <td>+ ; -</td>
            <td>%</td>
        </tr>
        </thead>
        <tbody>
        @foreach ($aggregated->getItems() as $item)
            @if($item->getTypeShowOnPlan() === 'false')
                <tr>
                    <td>
                        {{ $item->getParagraph() }}
                    </td>
                    <td>
                        {{ $item->getName() }}
                    </td>
                    @php
                        $number = 0;
                        while ($number < 22)
                        {
                            ++$number;
                            echo '<td></td>';
                        }
                    @endphp
                </tr>
            @else
            <tr>
                <td>
                    {{ $item->getParagraph() }}
                </td>
                <td>
                    {{ $item->getName() }}
                </td>
                <td>
                    {{ $item->getUnit() }}
                </td>
                <td>
                    {{ (float)number_format($item->getTotalFactLastYear(), 2, '.', '') }}
                </td>
                <td>
                    {{ (float)number_format($item->getTotalPlanCurrentYear(), 2, '.', '') }}
                </td>
                <td>
                    {{ (float)number_format($item->getTotalPlanFutureYear(), 2, '.', '') }}
                </td>

                @foreach($months as $monthId => $month)
                    <td title="{{ $item->getCommentForYear($monthId) }}"
                        style="@php echo(!empty($item->getCommentForYear($monthId)) ? 'background-color: #33CCFF;' : (!empty($item->getCommentDeviation($monthId)) ? 'background-color: #FFFF99;' : '')) @endphp">
                        {{ $item->getPlanOnYearFor($monthId) }}
                    </td>
                @endforeach

                <td>
                    {{ (float)number_format($item->getTotalPlanCurrentYear() - $item->getTotalFactLastYear(), 2, '.', '') }}
                </td>
                <td>
                    {{ (float)number_format($item->getPercentsDeviation(), 2, '.', '') }}
                </td>
            </tr>
            @endif
        @endforeach
        </tbody>
    </table>
@stop
