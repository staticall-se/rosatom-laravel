@extends('adminlte::page')

@section('title', 'Выбор отчётного года и шаблона')

@section('content')
    <div id='app'>
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Выберите шаблон и отчётный год</h3>
            </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br/>
                @endif
                    <form method="post" action="{{ route('plan.step2') }}">
                        @csrf
                        <div class="card-body">
                            <div class="col-3">
                                <div class="form-group" id="param_list">
                                    <label for="report_id">Шаблон</label>
                                    <select class="form-control js-params" id="report_id" name="report_id" required>
                                        <option value="" disabled selected hidden>Выберите шаблон...</option>
                                        @foreach ($reports as $report)
                                            <option value='{{ $report->company_id }}'>{{ $report->company->company_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group" id="param_list">
                                    <label for="year">Год</label>
                                    <select class="form-control js-params" id="year" name="year" required>
                                        <option value="" disabled selected hidden>Выберите год...</option>
                                        @foreach ($years as $year)
                                            <option id="{{ $year }}">{{ $year }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Перейти в таблицу для заполнения данных</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
@endsection
@section('page-js-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#company_id').select2();
            $('#report_id').select2();
            $('.js-params').select2();
        });
    </script>
@stop
