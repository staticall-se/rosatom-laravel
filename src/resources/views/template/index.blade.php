@extends('adminlte::page')

@section('title', 'Шаблоны отчётов')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1 class="display-3">Шаблоны отчётов</h1>
            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            @if(session()->get('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
            @endif
            <table class="table table-striped projects">
                <thead>
                <tr>
                    <td>МУП</td>
                    <td colspan="2"></td>
                </tr>
                </thead>
                <tbody>
                @foreach($reports as $report)
                    <tr>
                        <td>{{$report->company_id}}</td>
                        <td class="project-actions table-data__actions">
                            <a href="{{ route('template.edit', $report->id)}}" class="btn btn-primary">
                                Редактировать</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

