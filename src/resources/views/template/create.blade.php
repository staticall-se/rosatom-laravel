@extends('adminlte::page')

@section('title', 'Добавить шаблон отчёта')

@section('content')
    <div id='app'>
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Добавить шаблон отчёта</h3>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br/>
            @endif
            <form method="post" action="{{ route('template.store') }}">
                @csrf
                <div class="card-body">
                    <div class="col-3">
                        <div class="form-group">
                            <label for="company_id">МУП:</label>
                            <select class="form-control js-select" id="company_id" name="company_id" required>
                                <option value="" disabled selected hidden>Выберите МУП...</option>
                                @foreach ($companies as $company)
                                    <option id="{{ $company->id }}">{{ $company->company_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-7">
                        <div class="form-group">
                            <label for="params">Показатели:</label>
                            @foreach ($params as $param)
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" name="params[]" type="checkbox"
                                           id="params-{{ $param->id }}" value='{{ $param->id }}'>
                                    <label for="params-{{ $param->id }}"
                                           class="custom-control-label">{{ $param->paragraph }} {{ $param->name }}</label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="card-footer__custom">
                    <button type="submit" class="btn btn-primary">Добавить шаблон отчёта</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('page-js-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.js-select').select2();
        });
    </script>
@stop
