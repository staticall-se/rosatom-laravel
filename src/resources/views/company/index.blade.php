@extends('adminlte::page')

@section('title', ' МУПы')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1 class="display-3">Муниципальные предприятия</h1>
            <div>
                <a style="margin-bottom: 19px;" href="{{ route('company.create')}}" class="btn btn-primary">
                    Добавить МУП
                </a>
            </div>
            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            @if(session()->get('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
            @endif
            <table class="table table-striped">
                <thead>
                <tr>
                    <td>Название</td>
                    <td>Описание</td>
                    <td colspan="2"></td>
                </tr>
                </thead>
                <tbody>
                @foreach($companies as $company)
                    <tr>
                        <td>{{$company->company_name}}</td>
                        <td>{{$company->company_description}}</td>
                        <td class="table-data__actions">
                            <a class="btn btn-primary" href="{{ route('company.show',$company->id)}}">
                                Карточка МУП
                            </a>
                            <a href="{{ route('company.edit',$company->id)}}" class="btn btn-info">Редактировать</a>
                            <form action="{{ route('company.destroy', $company->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Удалить</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

