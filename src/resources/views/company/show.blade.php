@extends('adminlte::page')

@section('title', 'Просмотр  МУПа')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">{{ $company->company_name }}</h3>
        </div>
        <div class="card-body">
            <ul class="list-group list-group-flush">
                @if(!empty($company->company_name))
                    <li class="list-group-item">
                        <h5 class="card-title">Название МУПа: </h5>
                        <p class="card-text text-muted">
                            {{ $company->company_name }}
                        </p>
                    </li>
                @endif
                @if(!empty($company->company_description))
                    <li class="list-group-item">
                        <h5 class="card-title">Описание МУПа: </h5>
                        <p class="card-text text-muted">
                            {{ $company->company_description }}
                        </p>
                    </li>
                @endif
                @if(!empty($company->post_of_chief))
                    <li class="list-group-item">
                        <h5 class="card-title">Должность руководителя: </h5>
                        <p class="card-text text-muted">
                            {{ $company->post_of_chief }}
                        </p>
                    </li>
                @endif
                @if(!empty($company->fio_of_chief))
                    <li class="list-group-item">
                        <h5 class="card-title">ФИО руководителя: </h5>
                        <p class="card-text text-muted">
                            {{ $company->fio_of_chief }}
                        </p>
                    </li>
                @endif
                @if(!empty($company->post_of_contracting_officer))
                    <li class="list-group-item">
                        <h5 class="card-title">Должность должностного лица, заключившего с руководителем предприятия
                                               трудовой контракт: </h5>
                        <p class="card-text text-muted">
                            {{ $company->post_of_contracting_officer }}
                        </p>
                    </li>
                @endif
                @if(!empty($company->fio_of_contracting_officer))
                    <li class="list-group-item">
                        <h5 class="card-title">ФИО должностного лица, заключившего с руководителем предприятия трудовой
                                               контракт: </h5>
                        <p class="card-text text-muted">
                            {{ $company->fio_of_contracting_officer }}
                        </p>
                    </li>
                @endif
                @if(!empty($company->start_duration_contract))
                    <li class="list-group-item">
                        <h5 class="card-title">Срок действия начала трудового контракта с руководителем
                                               предприятия: </h5>
                        <p class="card-text text-muted">
                            {{ \Carbon\Carbon::parse($company->start_duration_contract)->format('d-m-Y') }}
                        </p>
                    </li>
                @endif
                @if(!empty($company->finish_duration_contract))
                    <li class="list-group-item">
                        <h5 class="card-title">Срок действия окончания трудового контракта с руководителем
                                               предприятия: </h5>
                        <p class="card-text text-muted">
                            {{ \Carbon\Carbon::parse($company->finish_duration_contract)->format('d-m-Y') }}
                        </p>
                    </li>
                @endif
                @if(!empty($company->location_address))
                    <li class="list-group-item">
                        <h5 class="card-title">Адресс: </h5>
                        <p class="card-text text-muted">
                            {{ $company->location_address }}
                        </p>
                    </li>
                @endif
                @if(!empty($company->telephone))
                    <li class="list-group-item">
                        <h5 class="card-title">Телефон: </h5>
                        <p class="card-text text-muted">
                            {{ $company->telephone }}
                        </p>
                    </li>
                @endif
                @if(!empty($company->fax))
                    <li class="list-group-item">
                        <h5 class="card-title">Факс: </h5>
                        <p class="card-text text-muted">
                            {{ $company->fax }}
                        </p>
                    </li>
                @endif
                @if(!empty($company->email))
                    <li class="list-group-item">
                        <h5 class="card-title">E-mail: </h5>
                        <p class="card-text text-muted">
                            {{ $company->email }}
                        </p>
                    </li>
                @endif
                @if(!empty($company->industry))
                    <li class="list-group-item">
                        <h5 class="card-title">Отрасль: </h5>
                        <p class="card-text text-muted">
                            {{ $company->industry }}
                        </p>
                    </li>
                @endif
                @if(!empty($company->main_activity))
                    <li class="list-group-item">
                        <h5 class="card-title">Основная активность: </h5>
                        <p class="card-text text-muted">
                            {{ $company->main_activity }}
                        </p>
                    </li>
                @endif
                @if(!empty($company->amount_statutory_fund))
                    <li class="list-group-item">
                        <h5 class="card-title">Размер уставного фонда:</h5>
                        <p class="card-text text-muted">
                            {{ $company->amount_statutory_fund }}
                        </p>
                    </li>
                @endif
                @if(!empty($company->post_of_executive_director))
                    <li class="list-group-item">
                        <h5 class="card-title">Должность исполнителя:</h5>
                        <p class="card-text text-muted">
                            {{ $company->post_of_executive_director }}
                        </p>
                    </li>
                @endif
                @if(!empty($company->fio_perpetrator))
                    <li class="list-group-item">
                        <h5 class="card-title">ФИО исполнителя:</h5>
                        <p class="card-text text-muted">
                            {{ $company->fio_perpetrator }}
                        </p>
                    </li>
                @endif
                @if(!empty($company->mobile_perpetrator))
                    <li class="list-group-item">
                        <h5 class="card-title">Телефон исполнителя:</h5>
                        <p class="card-text text-muted">
                            {{ $company->mobile_perpetrator }}
                        </p>
                    </li>
                @endif
                @if(!empty($company->email_perpetrator))
                    <li class="list-group-item">
                        <h5 class="card-title">E-mail исполнителя:</h5>
                        <p class="card-text text-muted">
                            {{ $company->email_perpetrator }}
                        </p>
                    </li>
                @endif
                @if(!empty($company->balance_board_review_date))
                    <li class="list-group-item">
                        <h5 class="card-title">Дата рассмотрения на балансовой комиссии:</h5>
                        <p class="card-text text-muted">
                            {{ \Carbon\Carbon::parse($company->balance_board_review_date)->format('d-m-Y') }}
                        </p>
                    </li>
                @endif
            </ul>
        </div>
    </div>
@endsection
