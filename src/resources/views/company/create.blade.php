@extends('adminlte::page')

@section('title', 'Добавить муниципалитет')

@section('content')
    <div class='col-md-6'>
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Добавить муниципальное предприятие</h3>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br/>
            @endif
            <form method="post" action="{{ route('company.store') }}">
                @csrf
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="company_name">Название МУПа:</label>
                            <input type="text" class="form-control" id='company_name' name="company_name"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="company_description">Описание МУПа: </label>
                            <input type="text" class="form-control" id='company_description' name="company_description">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="post_of_chief">Должность руководителя:</label>
                            <input type="text" class="form-control" id='post_of_chief' name="post_of_chief"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="fio_of_chief">ФИО руководителя:</label>
                            <input type="text" class="form-control" id='fio_of_chief' name="fio_of_chief">
                        </div>
                    </div>
                    <div class='form-row'>
                        <div class="form-group col-md-6">
                            <label for="post_of_contracting_officer">Должность должностного лица, заключившего с руководителем предприятия трудовой контракт:</label>
                            <input type="text" class="form-control" id="post_of_contracting_officer" name="post_of_contracting_officer"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="fio_of_contracting_officer">ФИО должностного лица, заключившего с руководителем предприятия трудовой контракт:</label>
                            <input type="text" class="form-control" id="fio_of_contracting_officer" name="fio_of_contracting_officer">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="start_duration_contract">Срок действия начала трудового контракта с руководителем предприятия:</label>
                            <input class="date form-control" type="text" name='start_duration_contract' id='start_duration_contract'>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="finish_duration_contract">Срок действия окончания трудового контракта с руководителем предприятия:</label>
                            <input class="date form-control" type="text" name='finish_duration_contract' id='finish_duration_contract'>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="location_address">Адрес местонахождения:</label>
                            <input type="text" class="form-control" id="location_address" name="location_address">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="telephone">Телефон:</label>
                            <input type="text" class="form-control" id="telephone" name="telephone">
                        </div>
                    </div>
                    <div class='form-row'>
                        <div class="form-group col-md-4">
                            <label for="fax">Факс:</label>
                            <input type="text" class="form-control" id="fax" name="fax">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="email">E-mail:</label>
                            <input type="text" class="form-control" id="email" name="email">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="industry">Отрасль:</label>
                            <input type="text" class="form-control" id="industry" name="industry">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="main_activity">Основной вид деятельности:</label>
                            <input type="text" class="form-control" id="main_activity" name="main_activity">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="amount_statutory_fund">Размер уставного фонда:</label>
                            <input type="text" class="form-control" id="amount_statutory_fund" name="amount_statutory_fund">
                        </div>
                    </div>
                    <div class='form-row'>
                        <div class="form-group col-md-6">
                            <label for="post_of_executive_director">Должность исполнителя:</label>
                            <input type="text" class="form-control" id="post_of_executive_director" name="post_of_executive_director">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="fio_perpetrator">ФИО исполнителя:</label>
                            <input type="text" class="form-control" id="fio_perpetrator" name="fio_perpetrator">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="mobile_perpetrator">Телефон исполнителя</label>
                            <input type="text" class="form-control" id="mobile_perpetrator" name="mobile_perpetrator">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email_perpetrator">E-mail исполнителя:</label>
                            <input type="email" class="form-control" id="email_perpetrator" name="email_perpetrator">
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Добавить муниципалитет</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('page-js-script')
    <script type="text/javascript">
        $('.date').datepicker({
            format: "dd-mm-yyyy",
            weekStart: 0,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            orientation: "auto",
            language: 'ru-RU'
        });
    </script>
@stop
