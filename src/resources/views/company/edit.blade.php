@extends('adminlte::page')

@section('title', 'Редактировать муниципалитет')

@section('content')
    <div class='col-md-6'>
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Обновить муниципальное предприятие: {{ $company->company_name }}</h3>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <br/>
            @endif
            <form method="post" action="{{ route('company.update', $company->id) }}">
                @method('PATCH')
                @csrf
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="company_name">Название муниципалитета:</label>
                            <input type="text" class="form-control" id='company_name' value='{{ $company->company_name}}' name="company_name"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="company_description">Описание муниципалитета: </label>
                            <input type="text" class="form-control" id='company_description' value='{{ $company->company_description}}' name="company_description">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="post_of_chief">Должность руководителя:</label>
                            <input type="text" class="form-control" id='post_of_chief' value='{{ $company->post_of_chief}}' name="post_of_chief"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="fio_of_chief">ФИО руководителя:</label>
                            <input type="text" class="form-control" id='fio_of_chief' value='{{ $company->fio_of_chief}}' name="fio_of_chief">
                        </div>
                    </div>
                    <div class='form-row'>
                        <div class="form-group col-md-6">
                            <label for="post_of_contracting_officer">Должность должностного лица, заключившего с руководителем предприятия трудовой контракт:</label>
                            <input type="text" class="form-control" id="post_of_contracting_officer" value='{{ $company->post_of_contracting_officer}}' name="post_of_contracting_officer"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="fio_of_contracting_officer">ФИО должностного лица, заключившего с руководителем предприятия трудовой контракт:</label>
                            <input type="text" class="form-control" id="fio_of_contracting_officer"value='{{ $company->fio_of_contracting_officer}}' name="fio_of_contracting_officer">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="start_duration_contract">Срок действия начала трудового контракта с руководителем предприятия:</label>
                            <input class="date form-control date-first" type="text" name='start_duration_contract' value='{{ $company->start_duration_contract}}' id='start_duration_contract'>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="finish_duration_contract">Срок действия окончания трудового контракта с руководителем предприятия:</label>
                            <input class="date form-control date-second" type="text" name='finish_duration_contract' value='{{ $company->finish_duration_contract}}' id='finish_duration_contract'>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="location_address">Адрес местонахождения:</label>
                            <input type="text" class="form-control" id="location_address" value='{{ $company->location_address }}' name="location_address">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="telephone">Телефон:</label>
                            <input type="text" class="form-control" id="telephone" value='{{ $company->telephone }}' name="telephone">
                        </div>
                    </div>
                    <div class='form-row'>
                        <div class="form-group col-md-4">
                            <label for="fax">Факс:</label>
                            <input type="text" class="form-control" id="fax" name="fax" value='{{ $company->fax }}'>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="email">E-mail:</label>
                            <input type="text" class="form-control" id="email" name="email" value='{{ $company->email }}'>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="industry">Отрасль:</label>
                            <input type="text" class="form-control" id="industry" name="industry" value='{{ $company->industry }}'>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="main_activity">Основной вид деятельности:</label>
                            <input type="text" class="form-control" id="main_activity" value='{{ $company->main_activity }}' name="main_activity">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="amount_statutory_fund">Размер уставного фонда:</label>
                            <input type="text" class="form-control" id="amount_statutory_fund" value='{{ $company->amount_statutory_fund }}' name="amount_statutory_fund">
                        </div>
                    </div>
                    <div class='form-row'>
                        <div class="form-group col-md-6">
                            <label for="post_of_executive_director">Должность исполнителя:</label>
                            <input type="text" class="form-control" id="post_of_executive_director" value='{{ $company->post_of_executive_director }}' name="post_of_executive_director">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="fio_perpetrator">ФИО исполнителя:</label>
                            <input type="text" class="form-control" id="fio_perpetrator" value='{{ $company->fio_perpetrator }}' name="fio_perpetrator">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="mobile_perpetrator">Телефон исполнителя</label>
                            <input type="text" class="form-control" id="mobile_perpetrator" value='{{ $company->mobile_perpetrator }}' name="mobile_perpetrator">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email_perpetrator">E-mail исполнителя:</label>
                            <input type="email" class="form-control" id="email_perpetrator" value='{{ $company->email_perpetrator }}' name="email_perpetrator">
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Обновить</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('page-js-script')
    <script type="text/javascript">
        $('.date').datepicker({
            format: "dd-mm-yyyy",
            weekStart: 0,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            orientation: "auto",
            language: 'ru-RU'
        });
        let dateFirst = new Date($('.date-first').attr('value') * 1000);
        let dateSecond = new Date($('.date-second').attr('value') * 1000);
        let convertDateFirst = dateFirst.getDate() + '-' + (dateFirst.getMonth() + 1) + '-' + dateFirst.getFullYear();
        let convertDateSecond = dateSecond.getDate() + '-' + (dateSecond.getMonth() + 1) + '-' + dateSecond.getFullYear();
        $('.date-first').datepicker('setDate', convertDateFirst);
        $('.date-second').datepicker('setDate', convertDateSecond);
    </script>
@stop
