@extends('adminlte::page')

@section('title', 'Просмотр коэфицента отклонения')

@section('content')
    <div class="col">
        <div class="mb-2">
            <h3>Текущий коофицент отклонения: {{ $percent }}%</h3>
        </div>
        <a href="{{ route('param.changePercentOfDeviation')}}" class="btn btn-info">
            Изменить текущий коофицент отклонения
        </a>
    </div>
@endsection
