@extends('adminlte::page')

@section('title', 'Изменение коэфицента отклонения')

@section('content')
    <div class="col-md-3">
        <h3>Изменить процент отклонения</h3>
        <div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <br/>
            @endif
            <form method="post" action="{{ route('param.updatePercentOfDeviation') }}">
                @csrf
                <div class="form-group">
                    <label for="percent">Процент отклонения:</label>
                    <input type="number" class="form-control" id="percent" name="percent" value='{{ $percent }}'/>
                </div>
                <button type="submit" class="btn btn-primary">Обновить</button>
            </form>
        </div>
    </div>
@endsection
