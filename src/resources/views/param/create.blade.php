@extends('adminlte::page')

@section('title', 'Добавить показатель')

@section('content')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-3">Добавить показатель</h1>
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br/>
                @endif
                <form method="post" action="{{ route('param.store') }}">
                    @csrf
                    <div class="form-group">
                        <label for="paragraph">Индекс:</label>
                        <input type="text" class="form-control" id="paragraph" name="paragraph" required/>
                    </div>
                    <div class="form-group">
                        <label for="name">Показатель:</label>
                        <input type="text" class="form-control" id="name" name="name" required/>
                    </div>
                    <div class="form-group">
                        <label for="unit">Единица измерения:</label>
                        <input type="text" class="form-control" id="unit" name="unit"/>
                    </div>
                    <div class="form-group">
                        <label>Выберите тип подсчёта:</label>
                        <div class="form-check">
                            <input class="form-check-input" value="sum" type="radio" name="type_counting">
                            <label class="form-check-label">Сумма</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" value="average" type="radio" name="type_counting">
                            <label class="form-check-label">Среднее значение</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Подсчитывать данные по параметру?</label>
                        <div class="form-check">
                            <input class="form-check-input" value="true" type="radio" name="show_on_plan">
                            <label class="form-check-label">Да</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" value="false" type="radio" name="show_on_plan">
                            <label class="form-check-label">Нет</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Добавить показатель</button>
                </form>
            </div>
        </div>
    </div>
@endsection
