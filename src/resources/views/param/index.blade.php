@extends('adminlte::page')

@section('title', 'Показатели')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1 class="display-3">Показатели</h1>
            <div>
                <a style="margin-bottom: 19px;" href="{{ route('param.create')}}" class="btn btn-primary">
                    Добавить показатель
                </a>
                <a style="margin-bottom: 19px;" href="{{ route('param.showPercentDeviation')}}" class="btn btn-info">
                    Посмотреть текущий коэфициент отклонения
                </a>
            </div>
            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif

            @if(session()->get('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
            @endif

            <table class="table table-striped">
                <thead>
                <tr>
                    <td>Индекс</td>
                    <td>Показатель</td>
                    <td>Единица измерения</td>
                    <td colspan="2"></td>
                </tr>
                </thead>
                <tbody>
                @foreach($params as $param)
                    <tr>
                        <td>{{$param->paragraph}}</td>
                        <td>{{$param->name}}</td>
                        <td>{{$param->unit}}</td>
                        <td class="table-data__actions">
                            <a href="{{ route('param.edit',$param->id)}}" class="btn btn-primary">Редактировать</a>
                            <form action="{{ route('param.destroy', $param->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Удалить</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div>
            </div>
            <div class="col-sm-12">
                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
            </div>
@endsection

