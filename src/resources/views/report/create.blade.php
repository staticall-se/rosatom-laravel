@extends('adminlte::page')

@section('title', 'Добавить шаблон отчёта')

@section('content')
    <div id='app'>
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Добавить данные в отчёт</h3>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br/>
            @endif
            <form method="post" action="{{ route('report.store') }}">
                @csrf
                <input type="hidden" name="company_id" value="{{ request()->get('id') }}">
                <div class="card">
                    <div class="card-body table-responsive p-0" style="height: 100vh;">
                        <table class="table table-head-fixed">
                            <thead>
                            <tr>
                                <th rowspan="2">Индекс</th>
                                <th rowspan="2">Показатель</th>
                                @foreach($months as $month)
                                    <th style="text-align: center;" colspan="1">{{ $month }}</th>
                                @endforeach
                            </tr>
                            <tr>
                                @foreach($months as $month)
                                    @if(in_array($month, $factMonth) || $month === $currentMonth)
                                        <th style="text-align: center; position: sticky; top: 48px; background-color: white;" colspan="1">
                                            факт
                                        </th>
                                    @else
                                        <th style="text-align: center; position: sticky; top: 48px; background-color: white;" colspan="1">
                                            прогноз
                                        </th>
                                    @endif
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($report as $item)
                                <tr>
                                    <td>{{ $item['paragraph'] }}</td>
                                    <td>{{ $item['name'] }}</td>
                                    @foreach($months as $monthId => $month)
                                        <td>
                                            @if($month === $currentMonth)
                                                <input class="form-control" style='width: 100px;' type="number" name="data[{{ $item['id'] }}][{{ $monthId }}][fact]"
                                                       value="{{ (empty($existingData[$item['id']][$monthId]['fact']) ? null : $existingData[$item['id']][$monthId]['fact']) }}">
                                                <textarea class='form-control mt-1' style='width: 100px;' placeholder="Комментарий к плану"
                                                          name="data[{{ $item['id'] }}][{{ $monthId }}][comment]">{{ (empty($existingData[$item['id']][$monthId]['comment']) ? null : $existingData[$item['id']][$monthId]['comment']) }}</textarea>
                                            @elseif(in_array($month, $factMonth, true))
                                                <input class="form-control" style='width: 100px;' type="number" name="data[{{ $item['id'] }}][{{ $monthId }}][fact]"
                                                       value="{{ (empty($existingData[$item['id']][$monthId]['fact']) ? null : $existingData[$item['id']][$monthId]['fact']) }}" disabled>
                                                <textarea class='form-control mt-1' style='width: 100px;' placeholder="Комментарий к плану"
                                                          name="data[{{ $item['id'] }}][{{ $monthId }}][comment]" disabled>{{ (empty($existingData[$item['id']][$monthId]['comment']) ? null : $existingData[$item['id']][$monthId]['comment']) }}</textarea>
                                            @else
                                                <input class="form-control" style='width: 100px;' type="number" name="data[{{ $item['id'] }}][{{ $monthId }}][prognosis]"
                                                       value="{{ (empty($existingData[$item['id']][$monthId]['prognosis']) ? null : $existingData[$item['id']][$monthId]['prognosis']) }}">
                                                <textarea class='form-control mt-1' style='width: 100px;' placeholder="Комментарий к плану"
                                                          name="data[{{ $item['id'] }}][{{ $monthId }}][comment]">{{ (empty($existingData[$item['id']][$monthId]['comment']) ? null : $existingData[$item['id']][$monthId]['comment']) }}</textarea>
                                            @endif
                                        </td>
                                    @endforeach
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer__custom">
                    <button type="submit" class="btn btn-primary">Добавить отчёт</button>
                </div>
            </form>
        </div>
    </div>
@endsection
