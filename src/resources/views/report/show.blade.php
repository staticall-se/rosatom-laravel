@extends('adminlte::page')

@section('title', 'Детальная информация')

@section('content_header')
    <h4>Отчёт по предприятию {{ $currentReport->company->company_name }} по основным
        показателям
        деятельности за {{ $currentYear }}</h4>
    <a class='btn btn-primary mt-2' href='{{ route('report.index') }}'>Вернуться</a>
@stop
<style>
    .content-wrapper {
        overflow: auto;
    }
</style>

@section('content')
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td rowspan="3">
                Индекс
            </td>
            <td rowspan="3">
                Показател
            </td>
            <td rowspan="3">
                Единица<br>измерения
            </td>
            <td colspan="18">
                Отчёт о выполнении плана на {{ $currentYear }} год
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" rowspan="2">
                Прогноз до конца {{ $currentYear }} год
            </td>
            @foreach($months as $monthId => $month)
                @if($currentMonth === $month)
                    <td style="text-align: center;" rowspan="2">
                        {{ $month }} </br>
                        (факт)
                    </td>
                @else
                    <td style="text-align: center;" rowspan="2">
                        {{ $month }} </br>
                        (прогноз)
                    </td>
                @endif
            @endforeach
            <td style="text-align: center;" colspan="2">
                Отклонение факта за {{$currentMonth}} от плана {{ $currentMonth }} (за отчетный месяц)
            </td>
            <td style="text-align: center;" colspan="2">
                Отклонение факта за {{$currentMonth}}(отчетный месяц) от плана {{ $currentMonth }} (нарастающим итогом)
            </td>
            <td rowspan="2">Причины отклонения (более {{ $percent }}%)</td>
        </tr>
        <tr>
            <td>+ ; -</td>
            <td>%</td>
            <td>+ ; -</td>
            <td>%</td>
        </tr>
        </thead>
        <tbody>
        @foreach ($aggregated->getItems() as $item)
            @if($item->getTypeShowOnPlan() === 'false')
                <tr>
                    <td>
                        {{ $item->getParagraph() }}
                    </td>
                    <td>
                        {{ $item->getName() }}
                    </td>
                    @php
                        $number = 0;
                        while ($number < 22)
                        {
                            ++$number;
                            echo '<td></td>';
                        }
                    @endphp
                    <td></td>
                </tr>
            @else
                <tr>
                    <td>
                        {{ $item->getParagraph() }}
                    </td>

                    <td>
                        {{ $item->getName() }}
                    </td>

                    <td>
                        {{ $item->getUnit() }}
                    </td>

                    <td>
                        {{ $item->getPrognosisOnCurrentYear() }}
                    </td>

                    @foreach($months as $monthId => $month)
                        <td title="{{ $item->getCommentForYear($monthId) }}"
                            style="@php echo(!empty($item->getCommentForYear($monthId)) ? 'background-color: #33CCFF;' : (!empty($item->getFactOnYearFor($monthId)) ? 'background-color: #FF6699;' : '') ) @endphp">
                            {{ $item->getFactOnYearFor($monthId) ?? $item->getPrognosisOnYearFor($monthId) }}
                        </td>
                    @endforeach

                    <td>
                        {{ $item->getDeviationFactFromPlanSum() }}
                    </td>

                    <td>
                        {{ $item->getDeviationFromPlanPercent() }}
                    </td>

                    <td>
                        {{ $item->getDeviationFactFromCurrentMonthSum() }}
                    </td>

                    <td>
                        {{ $item->getDeviationFactFromCurrentMonthPercent() }}
                    </td>

                    <td>
                        {{ $item->getCommentDeviation($cMonth) ?? 'Комментарий отсутствует' }}
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
@stop
