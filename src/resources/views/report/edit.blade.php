@extends('adminlte::page')

@section('title', 'Редактировать шаблон отчёта')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Обновить шаблон отчёта</h3>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <br/>
        @endif

        <form method="post" action="{{ route('report.update', $report->id) }}">
            @method('PATCH')
            @csrf

            <div class="card-body">
                <div class="col-3">
                    <div class="form-group">
                        <label for="company_id">Муниципалитет:</label>
                        <input type="text" id="company_id" class="form-control" name="company_id" value="{{ $report->company_id }}" disabled>
                    </div>
                </div>
                <div class="col-10">
                    <div class="form-group">
                        <label for="params">Показатели:</label>
                        @foreach ($params as $param)
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" name="params[]" type="checkbox" id="params-{{ $param->id }}" value='{{ $param->id }}' {{in_array($param->id, $reportCheckedIds) ? 'checked' : ''}}>
                                <label for="params-{{ $param->id }}" class="custom-control-label">{{ $param->paragraph }} {{ $param->name }}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="card-footer__custom">
                <button type="submit" class="btn btn-primary">Обновить</button>
            </div>
        </form>
    </div>
    </div>
@endsection
@section('page-js-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.js-select-company').select2({
                placeholder: 'Select an item',
            });
            let dataValueCompany = $('.js-select-company').attr('data-initial-value');
            $('.js-select-company').val(dataValueCompany);
        });
    </script>
@stop
