@extends('adminlte::page')

@section('title', 'Шаблоны отчётов')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h4 class="display-3">Отчёты</h4 class="display-3">
            <div class="row-cols-sm-5 mb-2">
                <div class="form-group">
                    <label for="month">Месяц</label>
                    <select class="form-control js-params" id="month"
                            onchange="document.location.href = '{{ route('report.index') }}?month=' + this.value">
                        <option value="{{ $currentMonth }}" disabled selected>{{ $currentMonth }}</option>
                        @foreach ($months as $month)
                            <option value="{{ $month }}">{{ $month }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            @if(session()->get('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
            @endif
            @if(count($reports) > 0)
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Предприятие</th>
                            <th>Статус</th>
                            <th>Дата утверждения</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($reports as $item)
                            <tr class='tr-custom'>
                                <td>{{ $item->ordinal }}</td>
                                <td class='bold'>{{ $item->company->company_name }}</td>
                                <td>
                            <span class="tag tag-entered bold">
                                Поступил
                            </span>
                                </td>
                                <td>{{ \Carbon\Carbon::parse($item->company->balance_board_review_date)->format('d.m.Y') }}</td>
                                <td class='bold'>
                                    @if(!empty($item->isCurrentYearPlan))
                                        <a href='#' class='btn btn-primary ml-2'>
                                            <i class="fas fa-print"></i>
                                            Печать в ПФХД
                                        </a>
                                    @endif
                                    @if(!empty($item->isCurrentYearPlan))
                                        <a href='{{ route('report.show', [$item->company->id, 'year'=> $currentYear]) }}'
                                           class='btn btn-info ml-2'>
                                            Просмотр отчёта
                                        </a>
                                    @endif
                                    @if(empty($item->isCurrentYearPlan))
                                        <a href="{{ route('report.create', ['id' => $item->company->id])}}" class="btn btn-primary ml-2">
                                            Создать отчёт
                                        </a>
                                    @else
                                        <a href="{{ route('report.create', ['id' => $item->company->id])}}" class="btn btn-primary ml-2">
                                            Обновить отчёт
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
    </div>
@endsection
@section('page-js-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.js-params').select2();
        });
    </script>
@stop
