<?php

return [

    'main_navigation'   => 'ГЛАВНОЕ МЕНЮ',
    'blog'              => 'Блог',
    'pages'             => 'Страницы',
    'account_settings'  => 'НАСТРОЙКИ ПРОФИЛЯ',
    'profile'           => 'Профиль',
    'change_password'   => 'Изменить пароль',
    'multilevel'        => 'Многоуровневое меню',
    'level_one'         => 'Уровень 1',
    'level_two'         => 'Уровень 2',
    'level_three'       => 'Уровень 3',
    'labels'            => 'Метки',
    'important'         => 'Важно',
    'warning'           => 'Внимание',
    'information'       => 'Информация',
    'title'             => 'Административные сущности',
    'page_municipality' => 'МУП',
    'page_indicators'   => 'Показатели',
    'page_templates'    => 'Шаблоны отчётов',
    'page_plan'         => 'План',
];
