<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index']);

Route::get('/assign_company', [App\Http\Controllers\CompanyController::class, 'assignParams'])->name('assign_company');

Route::get('/login', [\App\Http\Controllers\Auth\LoginController::class, 'login'])->name('login');
Route::get('/login/redirect', [\App\Http\Controllers\Auth\SocialController::class, 'loginRedirect']);

Route::prefix('param')->group(
    function () {
        Route::get('/percent-deviation', [\App\Http\Controllers\ParamController::class, 'percentOfDeviation'])->name('param.showPercentDeviation');
        Route::get('/change-deviation', [\App\Http\Controllers\ParamController::class, 'changePercentOfDeviation'])->name('param.changePercentOfDeviation');
        Route::post('/update-percent', [\App\Http\Controllers\ParamController::class, 'updatePercentOfDeviation'])->name('param.updatePercentOfDeviation');
    }
);

Route::prefix('plan')->group(
    function () {
        Route::get('/step-two', [\App\Http\Controllers\PlanController::class, 'step2'])->name('plan.step2');
        Route::get('/edit-plan', [\App\Http\Controllers\PlanController::class, 'editPlan'])->name('plan.editPlan');
    }
);

Route::prefix('report')->group(
    function () {
        Route::get('/show-report-month', [\App\Http\Controllers\ReportController::class, 'showReportCurMonth'])->name('report.showReport');
        Route::get('/show-report', [\App\Http\Controllers\ReportController::class, 'showReportForCorrect'])->name('report.showReportForCorrect');
    }
);

Route::resources(
    [
        'plan'    => \App\Http\Controllers\PlanController::class,
        'company' => \App\Http\Controllers\CompanyController::class,
        'param'   => \App\Http\Controllers\ParamController::class,
        'report'  => \App\Http\Controllers\ReportController::class,
        'template'  => \App\Http\Controllers\TemplateController::class,
    ]
);
